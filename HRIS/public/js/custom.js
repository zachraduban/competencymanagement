function doDate()
{
    var str = "";

    var days = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
    var months = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

    var now = new Date();

    str += days[now.getDay()] + ", " + now.getDate() + " " + months[now.getMonth()] + " " + now.getFullYear() + " " + now.getHours() +":" + now.getMinutes() + ":" + now.getSeconds();
    document.getElementById("standardTime").innerHTML = str;
}

setInterval(doDate, 0);

function setTokenCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getTokenCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function storeToken(token) {
	setTokenCookie("sessionToken", token, 1);
}

function storeDetails(details) {
    setTokenCookie("userDetails", details, 1);
}

function storeSupervisorDetails(details) {
    setTokenCookie("supervisorDetails", details, 1);
}

function storeRole(role) {
    setTokenCookie("role", role, 1);
}

function logout() {
	document.cookie = "sessionToken=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
  document.cookie = "userDetails=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
  document.cookie = "supervisorDetails=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
  document.cookie = "role=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
	window.location.href="login.html";
}

function stripString(text) {
    var newString = text.toLowerCase().replace(/[^A-Z0-9]/ig, '');
    return newString;
}

(function($) {
  $.fn.inputFilter = function(inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      }
    });
  };
}(jQuery));

$('.capitalize').keyup(function() {
  $(this).val($(this).val().toUpperCase());
});

$('.numeric').inputFilter(function(value) {
  return /^\d*$/  .test(value); 
});

$('.alphabet').inputFilter(function(value) {
  return /^[A-Z a-z]*$/  .test(value); 
});

function init(competencypage) {
    var token = 'Bearer ' + getTokenCookie("sessionToken");
    var userDetails = getTokenCookie("userDetails");

    if (JSON.parse(userDetails).first_name != null) {
      $('#welcomesign').text('Welcome ' + JSON.parse(userDetails).first_name + '!');
    } else {
      $('#welcomesign').text('Welcome ' + JSON.parse(userDetails).username + '!');
    }

    var role = getTokenCookie("role");
    if (role == 'admin') {
      $('.admin-view').removeClass("hide");
      $('.admin-view').show();
    } else if (role == 'HRMO') {
      $('.hrmo-view').removeClass("hide");
      $('.hrmo-view').show();
    } else if (role == 'Supervisor') {
      $('.supervisor-view').removeClass("hide");
      $('.supervisor-view').show();
    } else if (role == 'Employee') {
      $('.employee-view').removeClass("hide");
      $('.employee-view').show();
    }

    $.ajax({
      url:'crm/notification/numberOfRequest?',
      headers: {
        'Authorization': token
      },
      type: 'GET',
      dataType: 'json',
      success: function(data) {

        var numSignupRequests = data.numberOfRequestForApproval != null ? data.numberOfRequestForApproval : 0;
        var numChangeRequests = data.numberOfRequestToChangePlaceAssignment != null ? data.numberOfRequestToChangePlaceAssignment : 0;
        totalRequests = numSignupRequests + numChangeRequests;

        if (totalRequests > 0) {
          $('.total-requests').prepend('<span class="new red badge">'+totalRequests+'</span>');
        }

        if (numSignupRequests > 0) {
          $('.approval-requests').prepend('<span class="new red badge">'+numSignupRequests+'</span>');
        }

        if (numChangeRequests > 0) {
          $('.change-requests').prepend('<span class="new red badge">'+numChangeRequests+'</span>');
        }
      }
    })


    $.ajax({
      url:'crm/competency/list?',
      headers: {
        'Authorization': token
      },
      type: 'GET',
      dataType: 'json',
      success: function(data) {
        if (data.locking_assessment_form == 0) {
          $('.assessment-tab').removeClass("disabled");
        }

        if(data.employee_submitted == 0 || data.employee_submitted == null) {
          $('.submitBtn').show();
        }
        $.each(data, function(i, value) {
          if(value instanceof Object) {
              var path = stripString(i) + '.html';
              if (competencypage == stripString(i)) {
                $('#competencies-sidebar ul').append($('<li class="active">').append($('<a>').text(i)
                    .attr('href', path)));

                $.each(value, function(j, val) {
                  // console.log(i + " " + val);
                  if(val instanceof Object) {
                    //append cards in list
                    var competencies = $('#' + stripString(i) + ' ul');
                    competencies.append($('<li>')
                        .append($('<div class="collapsible-header">').text(j))
                        .append($('<div class="collapsible-body">')
                            .append(
                                $('<form class="competencyform">')
                            )
                        )
                    );
                    
                    // console.log(i);
                    // console.log(val);

                    for(var k in val.behaviorals) {
                        // console.log(val.behaviorals[k])
                        var index = (k * 1) + 1;
                        var id = val.behaviorals[k].behavioral_id;
                        var name = val.behaviorals[k].behavioral_name;
                        var employee_rate = val.behaviorals[k].employee_rate;//employee rate compare with id of input
                        competencies.find("li:last form").append(
                            $('<div class="row"></div>').append(
                                $('<div class="cell"></div>').append(
                                    $('<span></span>').text(name)
                                )
                            ).append(
                                $('<div class="cell"></div>').append(
                                    $('<div class="box"></div>').append(
                                        $('<div class="rating"></div>').append(
                                            $('<input type="radio" name="'+id+'" id="rate'+((id*5)+1)+'" value="5">')
                                        ).append(
                                            $('<label for="rate'+((id*5)+1)+'"></label>').text("5")
                                        ).append(
                                            $('<input type="radio" name="'+id+'" id="rate'+((id*5)+2)+'" value="4">')
                                        ).append(
                                            $('<label for="rate'+((id*5)+2)+'"></label>').text("4")
                                        ).append(
                                            $('<input type="radio" name="'+id+'" id="rate'+((id*5)+3)+'" value="3">')
                                        ).append(
                                            $('<label for="rate'+((id*5)+3)+'"></label>').text("3")
                                        ).append(
                                            $('<input type="radio" name="'+id+'" id="rate'+((id*5)+4)+'" value="2">')
                                        ).append(
                                            $('<label for="rate'+((id*5)+4)+'"></label>').text("2")
                                        ).append(
                                            $('<input type="radio" name="'+id+'" id="rate'+((id*5)+5)+'" value="1">')
                                        ).append(
                                            $('<label for="rate'+((id*5)+5)+'"></label>').text("1")
                                        )
                                    ).append(data.employee_submitted == 1 ? '' : (index == val.behaviorals.length ? '<br><br><a href="#" class="saveBtn btn waves-effect waves-light yellow right black-text"><i class="material-icons right">keyboard_arrow_right</i>SAVE & NEXT</a>' : ''))
                                )
                            )
                        )
                        competencies.find('input[value=' + employee_rate + ']').last().prop("checked", true)
                    }
                  }
                });
              } else {
                $('#competencies-sidebar ul').append($('<li>').append($('<a>').text(i).attr('href', path)));
              }
          } 
        });
      },
      error: function(data) {
        logout();
      }
    });
}

function viewEmployeeProfile(employee_id, first_name, last_name, position, designation) {
    window.location.replace("viewemployee.html?employee_id=" + employee_id + "&first_name=" + first_name + "&last_name=" + last_name + "&position=" + position + "&designation=" + designation);
}

function getParam(address, param) {
    var url = new URL(address);
    var c = url.searchParams.get(param);

    return c;
}

function generateProfileGraph(competency, container_id, contextElements, scores) {
    $('#competencyAverages').append(
        '<div id="container'+ container_id + '" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto">'
    )
    Highcharts.chart('container'+ container_id, {
      chart: {
          type: 'bar'
      },
      title: {
          text: competency
      },
      xAxis: {
          categories: contextElements,
          title: {
              text: 'Context Elements'
          }
      },
      yAxis: {
          min: 0,
          max: 5,
          title: {
              text: 'Level',
              align: 'high'
          },
          labels: {
              overflow: 'justify'
          },
          allowDecimals: false
      },
      plotOptions: {
          bar: {
              dataLabels: {
                  enabled: true
              }
          }
      },
      legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'top',
          x: -40,
          y: 80,
          floating: true,
          borderWidth: 1,
          backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
          shadow: true
      },
      credits: {
          enabled: false
      },
      series: [{
          name: 'Level',
          data: scores
      }]
    });
}

function deactivateEmployeeAccount(employee_id, event) {
  if (confirm("Are you sure you want to deactivate this employee's account? Deactivating this account will prevent the employee from logging into their account and will not be able submit their competency assessment and view their data.")) {
    var token = 'Bearer ' + getTokenCookie("sessionToken");
    $.ajax({
      url:'crm/user/deactivate?',
      headers: {
        'Authorization': token
      },
      data: {"employee_id": employee_id},
      type: 'POST',
      success: function(data) {
        if (data.success == "true") {
          alert("Account Deactivated Successfully.");
          event.srcElement.closest('tr').remove();
        } else {
          alert(data.message);
        } 
      },
      error: function(data) {
        alert("Error! Check for missing fields.");
      }
    });
  }
}
