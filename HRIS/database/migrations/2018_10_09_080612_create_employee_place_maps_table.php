<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeePlaceMapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_place_maps', function (Blueprint $table) {
            $table->increments('map_id');
            
            $table->integer('employee_id')->unsigned();
            if (Schema::hasTable('employees')) {
              $table->foreign('employee_id')
                    ->references('employee_id')->on('employees')
                    ->onDelete('cascade');
            }
            
            $table->integer('region_id')->unsigned();
            if (Schema::hasTable('region_assignments')) {
              $table->foreign('region_id')
                    ->references('region_id')->on('region_assignments')
                    ->onDelete('cascade');
            }
            
            $table->integer('province_id')->unsigned();
            if (Schema::hasTable('province_assignments')) {
            $table->foreign('province_id')
                  ->references('province_id')->on('province_assignments')
                  ->onDelete('cascade');
            }
            
            $table->integer('city_id')->unsigned();
            if (Schema::hasTable('city_assignments')) {
              $table->foreign('city_id')
                    ->references('city_id')->on('city_assignments')
                    ->onDelete('cascade');
            }
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_place_maps');
    }
}
