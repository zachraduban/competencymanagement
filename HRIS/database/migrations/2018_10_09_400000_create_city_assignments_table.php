<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCityAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city_assignments', function (Blueprint $table) {
            $table->increments('city_id');
            $table->integer('city_code')->unique();
            $table->string('city_name');
            $table->integer('province_id')->unsigned();
            if (Schema::hasTable('province_assignments')) {
                $table->foreign('province_id')
                      ->references('province_id')->on('province_assignments')
                      ->onDelete('cascade');
            }
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('city_assignments');
    }
}
