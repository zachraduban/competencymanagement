<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvinceAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('province_assignments', function (Blueprint $table) {
            $table->increments('province_id');
            $table->integer('province_code')->unique();
            $table->string('province_name');
            $table->integer('region_id')->unsigned();
            if (Schema::hasTable('region_assignments')) {
                $table->foreign('region_id')
                      ->references('region_id')->on('region_assignments')
                      ->onDelete('cascade');
            }      
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('province_assignments');
    }
}
