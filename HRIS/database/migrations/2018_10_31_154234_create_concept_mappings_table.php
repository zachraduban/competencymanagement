<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConceptMappingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('concept_mappings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('concept_competencies_id');
            $table->string('concept_contexts_id');
            $table->string('concept_behavioral_id');
            $table->string('positions_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('concept_mappings');
    }
}
