<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('employee_id');
            $table->string('surname');
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('extension');
            $table->dateTime('date_of_birth');
            $table->string('place_of_birth');
            $table->string('sex');
            $table->string('civil_status');
            $table->binary('avatar');
            $table->string('street');
            $table->string('city_or_municipality');
            $table->string('barangay');
            $table->string('barangay');
            $table->integer('tel_no');
            $table->integer('mobile_no');
            $table->string('username');
            $table->string('password')->unique();
            $table->rememberToken();
            $table->integer('region_id')->unsigned();
            if (Schema::hasTable('region_assignments')) {
              $table->foreign('region_id')
                    ->references('region_id')->on('region_assignments')
                    ->onDelete('cascade');
            }
            
            $table->integer('province_id')->unsigned();
            if (Schema::hasTable('province_assignments')) {
            $table->foreign('province_id')
                  ->references('province_id')->on('province_assignments')
                  ->onDelete('cascade');
            }
            
            $table->integer('city_id')->unsigned();
            if (Schema::hasTable('city_assignments')) {
              $table->foreign('city_id')
                    ->references('city_id')->on('city_assignments')
                    ->onDelete('cascade');
            }
            $table->string('designation');
            $table->integer('salary_grade');
            $table->string('position');
            $table->integer('supervisor');
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
