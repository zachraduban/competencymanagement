<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeTrainingAttended extends Model
{
    //

    protected $fillable = [
    'employee_id',
    'name_of_training',
    'number_of_hours',
    'organizer',
    'year_attended',
    ];

    protected $primaryKey = 'training_attended_id';

    protected $hidden = ['updated_at', 'created_at', 'deleted_at'];
}
