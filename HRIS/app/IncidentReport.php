<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncidentReport extends Model
{
    //
    protected $primaryKey = 'incident_id';

    protected $hidden = ['updated_at', 'deleted_at'];
}
