<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConceptMapping extends Model
{
    //
    protected $hidden = ['positions_id','created_at', 'updated_at', 'deleted_at'];

    public function competencies(){
        return $this->hasMany('App\ConceptCompetency');
    }

    public function childCompetencies(){
    	return $this->hasMany('App\ConceptChildCompetency');
    }

    public function contexts(){
    	return $this->hasMany('App\ConceptContext');
    }

    public function behaviorals(){
        return $this->hasMany('App\ConceptBehavioral');
    }
}
