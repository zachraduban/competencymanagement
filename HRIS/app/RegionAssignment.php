<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegionAssignment extends Model
{
    //
    protected $fillable = [
    	'regionName',
        'regionCode'
    ];

    protected $hidden = ['created_at', 'updated_at'];

    public function employee(){
    	return $this->hasMany('App\Employee');
    }

    public function province(){
    	return $this->hasMany('App\provinceAssignment');
    }
}
