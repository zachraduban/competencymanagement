<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    //
    protected $hidden = ['designation_id','created_at', 'updated_at'];

    public function employee(){
    	return $this->hasMany('App\Employee');
    }
}
