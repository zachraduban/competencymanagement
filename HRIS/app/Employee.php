<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Authenticatable
{
    //

    use SoftDeletes;

    protected $primaryKey = 'employee_id';

    protected $hidden = ['created_at', 'updated_at', 'designation_id', 'deleted_at'];

    public function region(){
        return $this->belongsTo('App\RegionAssignment', 'regionId', 'regionId');
    }

    public function province(){
        return $this->belongsTo('App\ProvinceAssignment', 'provinceId', 'provinceId');
    }

    public function city(){
        return $this->belongsTo('App\CityAssignment', 'cityId', 'cityId');
    }

    public function designation(){
        return $this->belongsTo('App\Designation', 'designation_id', 'designation_id');
    }

    public function position(){
        return $this->belongsTo('App\Position', 'position_id', 'position_id');
    }
}
