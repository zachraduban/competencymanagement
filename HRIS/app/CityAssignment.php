<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityAssignment extends Model
{
    //
    protected $fillable = [
    	'cityName',
        'provinceId'
    ];

    protected $hidden = ['created_at', 'updated_at'];

    public function employee(){
    	return $this->hasMany('App\Employee');
    }

    public function provinces(){
    	return $this->belongsTo('App\provinceAssignment', 'provinceId', 'provinceId');
    }
}
