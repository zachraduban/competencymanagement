<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IndividualDevelopmentPlan extends Model
{
    //
	protected $fillable = [
	'employee_id',
	'short_term_career_goals',
	'long_term_career_goals',
	'target_competency',
	'dev_activity_needed',
	'outcome_desired',
	'support_needed',
	'amount_needed',
	'intended_completion_date',
	'supervisor_id',
	'employee_submitted',
	'supervisor_submitted'
	];

	protected $primaryKey = 'idp_id';

	protected $hidden = ['updated_at', 'created_at', 'deleted_at'];
}
