<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeePlaceMap extends Model
{
    protected $fillable = [
        'employeeId',
        'regionId',
        'provinceId',
        'cityId'
    ];
    //
    public function employee(){
    	return $this->belongsTo('App\Employee', 'employeeId', 'employeeId');
    }

    public function region(){
    	return $this->belongsTo('App\regionAssignment', 'regionId', 'regionId');
    }

    public function province(){
    	return $this->belongsTo('App\provinceAssignment', 'provinceId', 'provinceId');
    }

    public function city(){
    	return $this->belongsTo('App\cityAssignment', 'cityId', 'cityId');
    }
}
