<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    //
    protected $hidden = ['created_at', 'updated_at'];

    public function employee(){
    	return $this->hasMany('App\Employee');
    }
}
