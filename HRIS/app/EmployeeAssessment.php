<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeAssessment extends Model
{
    //

    protected $fillable = [
    	'behavioral_id',
        'employee_rate',
        'employee_id',
        'employee_is_submitted',
        'supervisor_rate',
        'supervisor_id',
        'supervisor_is_submitted',
        'created_at'
    ];

    protected $primaryKey = 'assessment_id';

    protected $hidden = ['updated_at'];
}
