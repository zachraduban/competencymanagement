<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConceptContext extends Model
{
    //
    protected $hidden = ['created_at', 'updated_at'];

    public function childCompetency(){
    	return $this->belongsTo('App\ConceptChildCompetency');
    }

    public function conceptMappings(){
    	return $this->belongsTo('App\ConceptMapping');
    }
}
