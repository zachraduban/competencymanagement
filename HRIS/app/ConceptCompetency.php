<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConceptCompetency extends Model
{
    //
    protected $hidden = ['created_at', 'updated_at'];

    public function childCompetencies(){
    	return $this->hasMany('App\ConceptChildCompetencies');
    }

    public function contexts(){
    	return $this->hasMany('App\ConceptContext');
    }

    public function conceptMappings(){
    	return $this->belongsTo('App\ConceptMapping');
    }
}
