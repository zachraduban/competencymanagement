<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempPlaceOfAssignment extends Model
{
    //
    protected $primaryKey = 'assignment_id';

    protected $hidden = ['updated_at', 'created_at', 'deleted_at'];
}
