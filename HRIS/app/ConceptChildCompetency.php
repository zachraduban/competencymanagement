<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConceptChildCompetency extends Model
{
    //
    protected $hidden = ['created_at', 'updated_at'];

    public function competency(){
    	return $this->belongsTo('App\ConceptCompetency');
    }

    public function conceptMappings(){
    	return $this->belongsTo('App\ConceptMapping');
    }
}
