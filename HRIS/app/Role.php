<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    protected $fillable = [
    'role_name'];

    protected $primaryKey = 'role_id';

    protected $hidden = ['updated_at', 'created_at'];
}
