<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProvinceAssignment extends Model
{
    //
    protected $fillable = [
    	'provinceName',
        'regionId',
        'regionCode'
    ];

    protected $hidden = ['created_at', 'updated_at'];

    public function employee(){
    	return $this->hasMany('App\Employee');
    }

    public function regions(){
    	return $this->belongsTo('App\regionAssignment');
    }

    public function cities(){
    	return $this->hasMany('App\cityAssignment');
    }
}
