<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConceptBehavioral extends Model
{
    //

    public function conceptMappings(){
    	return $this->belongsTo('App\ConceptMapping');
    }
}
