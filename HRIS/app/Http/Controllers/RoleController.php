<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;

use App\Employee;
use App\User;
use App\Notification;
use App\TempPlaceOfAssignment;

class RoleController extends Controller
{

	public function countRequestForApproval(Request $request){

		if($request->headers->has('Authorization')){

			$token = explode(' ', $request->header('Authorization'));
			$user = User::where('remember_token', $token[1])->first();
			$employee = Employee::where('user_id', '=', $user->user_id)->first();

			if($employee){

				if($user->role_id == 2){

					$count = DB::table('employees')
					->where('employees.region_code', $employee->region_code)
					->where('hrmo_is_approved', 0)
					->where('employees.designation_id', '!=', 15)
					->count();

					$countChangeRequest = TempPlaceOfAssignment::where('temp_place_of_assignments.region_code', $employee->region_code)
					->count();

					return response()->json([
						'numberOfRequestForApproval' => $count,
						'numberOfRequestToChangePlaceAssignment' => $countChangeRequest]);

				}elseif ($user->role_id == 3) {
					
					$count = Notification::where('supervisor_id', $employee->employee_id)
					->where('is_approved', 0)
					->count();

					return response()->json([
						'numberOfRequestForApproval' => $count]);
				}

			}
		}
	}
}
