<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use \Datetime;

use App\Employee;
use App\ConceptCompetency;
use App\ConceptContext;
use App\ConceptChildCompetency;
use App\ConceptMapping;
use App\ConceptBehavioral;
use App\EmployeeAssessment;
use App\User;


class CompetencyController extends Controller
{
    //

	public function getCompetencyByPosition(Request $request){
		$competencyMap = '';
		$competency = [];
		if($request->headers->has('Authorization')){
			$token = explode(' ', $request->header('Authorization'));
			$user = User::where('remember_token', $token[1])->first();
			$employee = Employee::where('user_id', '=', $user->user_id)->first();

			if($employee){
				$competencyMap = ConceptMapping::where('positions_id', 'like', '%'.$employee->position_id.'%')->first();
				
				$array1 = json_decode($competencyMap->concept_competencies_id, true);
				$array2 = json_decode($competencyMap->concept_child_competencies_id, true);;
				$array3 = json_decode($competencyMap->concept_behavioral_id, true);

				return $this->getCompetencyAssessmentByPosition($array1, $array3, $employee->employee_id);
				
			} else {
				return response()->json(['success' => 'false',
					'message' => 'User Not Found']);
			}
		}else{
			
			return response()->json(['success' => 'false',
				'message' => 'No User Authentication Founded']);
		}
	}

	public function saveCompetency(Request $request){

		if($request->headers->has('Authorization')){
			$token = explode(' ', $request->header('Authorization'));
			$user = User::where('remember_token', $token[1])->first();
			$employee = Employee::where('user_id', '=', $user->user_id)->first();

			$startDate = DateTime::createFromFormat('m/d', '07/01')->format('m-d');
			$endDate = DateTime::createFromFormat('m/d', '08/31')->format('m-d');
			$todayDate = date("m-d");

			if($user){
				$data = $request->json()->all();

				if($todayDate >= $startDate && $todayDate <= $endDate){

					if($request->exists('employee_id')){
					// return "HELLo";
						$this->saveCompetencyBySuperVisor($data, $employee, $request->employee_id, 'Save');
					}else{
						$this->saveCompetencySelfAssessment($data, $employee, 'Save');
					}

				}else{

					return response()->json(['success' => 'false',
						'message' => 'Assessment form is locked',
						'locking_assessment_form' => 1]);
				}

				return response()->json(['success' => 'true',
					'message' => 'Successfully Save',
					'status' => 0]);

			} else {
				return response()->json(['success' => 'false',
					'message' => 'User Not Found']);
			}

		}else{
			
			return response()->json(['success' => 'false',
				'message' => 'No User Authentication Founded']);
		}

	}

	public function submitCompetency(Request $request){

		if($request->headers->has('Authorization')){
			$token = explode(' ', $request->header('Authorization'));
			$user = User::where('remember_token', $token[1])->first();
			$employee = Employee::where('user_id', '=', $user->user_id)->first();

			if($user){

				if($request->exists('employee_id')){
					return $this->submitCompetencyBySuperVisor($request->employee_id);
				}else{
					return $this->submitCompetencySelfAssessment($employee);
				}

			} else {
				return response()->json(['success' => 'false',
					'message' => 'User Not Found']);
			}

		}else{

			return response()->json(['success' => 'false',
				'message' => 'No User Authentication Founded']);
		}

	}

	public function checkEmployeeAssessment(Request $request){

		if($request->headers->has('Authorization')){
			$token = explode(' ', $request->header('Authorization'));
			$user = User::where('remember_token', $token[1])->first();
			$employee = Employee::where('user_id', '=', $user->user_id)->first();

			if($employee){
				$employeeId = Employee::where('employee_id', $request->employee_id)->first();

				$competencyMap = ConceptMapping::where('positions_id', 'like', '%'.$employeeId->position_id.'%')->first();

				$array1 = json_decode($competencyMap->concept_competencies_id, true);
				$array2 = json_decode($competencyMap->concept_child_competencies_id, true);
				$array3 = json_decode($competencyMap->concept_behavioral_id, true);

				return $this->getCompetencyAssessmentByPosition($array1, $array3, $request->employee_id);

			} else {
				return response()->json(['success' => 'false',
					'message' => 'User Not Found']);
			}
		}else{

			return response()->json(['success' => 'false',
				'message' => 'No User Authentication Founded']);
		}
	}

	public function saveCompetencySelfAssessment($data, $user, $status){
		if($status == 'Save'){

			foreach ($data as $data2) {
				$createdYear = EmployeeAssessment::where('employee_id', '=', $user->employee_id)->where('behavioral_id', $data2['behavioral_id'])
				->whereYear('employee_assessments.updated_at', '=', date('Y'))->first();

				EmployeeAssessment::updateOrCreate(
					['employee_id' => $user->employee_id,
					'behavioral_id' => $data2['behavioral_id'],
					'created_at' => $createdYear->created_at ?? null
					],
					[
					'behavioral_id' => $data2['behavioral_id'], 
					'employee_rate' => $data2['rate'],
					'employee_id' => $user->employee_id,
					'employee_is_submitted' => 0]
					);

			}
		}else{
			$submit_array = ['employee_is_submitted' => 1];
			$submittedCompetency = EmployeeAssessment::where('employee_id', $user->employee_id)->update($submit_array);
		}
	}

	public function submitCompetencySelfAssessment($user){

		$countBehavioralId = EmployeeAssessment::where('employee_id', $user->employee_id)
		->whereYear('updated_at', '=', date('Y'))->get();

		$employeeNullArray = [];

		foreach ($countBehavioralId as $key => $value) {
			if(empty($value['employee_is_submitted'])){
				$employeeNullArray[] = $value['behavioral_id'];
			}
		}

		$competencyMap = ConceptMapping::where('positions_id', 'like', '%'.$user->position_id.'%')
		->select('concept_behavioral_id')
		->first();

		$convertToArrayBehavioralId = json_decode($competencyMap->concept_behavioral_id, true);

		if(count($convertToArrayBehavioralId) == count($employeeNullArray)){
			$submit_array = ['employee_is_submitted' => 1];	
			$submittedCompetency = EmployeeAssessment::where('employee_id', '=', $user->employee_id)->update($submit_array);
		}else{
			return response()->json(['success' => 'false',
				'message' => 'Missing rate',
				'status' => 0]);
		}

		return response()->json(['success' => 'true',
			'message' => 'Successfully Submitted',
			'status' => 1]);
	}

	public function saveCompetencyBySuperVisor($data, $user, $id, $status){

		if($status == 'Save'){

			foreach ($data['behaviorals'] as $data2) {

				$createdYear = EmployeeAssessment::where('employee_id', '=', $id)->where('behavioral_id', $data2['behavioral_id'])
				->whereYear('employee_assessments.updated_at', '=', date('Y'))->first();

				EmployeeAssessment::updateOrCreate(
					['employee_id' => $id,
					'behavioral_id' => $data2['behavioral_id'],
					'created_at' => $createdYear->created_at ?? null],
					[
					'behavioral_id' => $data2['behavioral_id'], 
					'supervisor_rate' => $data2['rate'],
					'supervisor_id' => $user->employee_id,
					'supervisor_is_submitted' => 0]
					);

			}
		}else{
			$submit_array = ['supervisor_is_submitted' => 1];
			$submittedCompetency = EmployeeAssessment::where('employee_id', $id)->update($submit_array);
		}
	}

	public function submitCompetencyBySuperVisor($id){

		$countBehavioralId = EmployeeAssessment::where('employee_id', $id)
		->whereYear('updated_at', '=', date('Y'))->get();

		$supervisorNullArray = [];

		foreach ($countBehavioralId as $key => $value) {
			if(empty($value['supervisor_is_submitted'])){
				$supervisorNullArray[] = $value['behavioral_id'];
			}
		}

		$employeeId = Employee::where('employee_id', $id)->first();

		$competencyMap = ConceptMapping::where('positions_id', 'like', '%'.$employeeId->position_id.'%')
		->select('concept_behavioral_id')
		->first();

		$convertToArrayBehavioralId = json_decode($competencyMap->concept_behavioral_id, true);

		if(count($convertToArrayBehavioralId) == count($supervisorNullArray)){
			$submit_array = ['supervisor_is_submitted' => 1];

			$submittedCompetency = EmployeeAssessment::where('employee_id', $id)->update($submit_array);
		}else{
			return response()->json(['success' => 'false',
				'message' => 'Missing rate',
				'status' => 0]);
		}

		return response()->json(['success' => 'true',
			'message' => 'Successfully Submitted',
			'status' => 1]);
	}


	//calling function for getting competency by position
	public function getCompetencyAssessmentByPosition($array1, $array2, $id){

		$competencyMap = '';
		$competency = [];

		$data = DB::table('concept_behaviorals')
		->leftJoin(DB::raw('(SELECT avg(employee_rate) AS employeeAverage, ANY_VALUE(employee_rate) AS employee_rate, avg(supervisor_rate) AS supervisorAverage, ANY_VALUE(supervisor_rate) AS supervisor_rate, ANY_VALUE(assessment_id) AS assessment_id, ANY_VALUE(employee_id) AS employee_id, ANY_VALUE(employee_assessments.behavioral_id) AS behavioral_id FROM employee_assessments LEFT JOIN (concept_behaviorals AS cb) ON employee_assessments.behavioral_id = cb.behavioral_id JOIN (concept_child_competencies) ON concept_child_competencies.concept_child_competency_id = cb.concept_child_competency_id WHERE
			(employee_assessments.employee_id = '.$id.'
			OR employee_assessments.employee_id IS NULL) AND YEAR(employee_assessments.updated_at) = YEAR(CURDATE()) GROUP BY concept_child_competencies.concept_child_competency_id, employee_assessments.employee_id) AS assess'), function($join){

			$join->on('assess.behavioral_id', '=', 'concept_behaviorals.behavioral_id');
		})
		->leftJoin('concept_child_competencies', function ($join){
			$join->on('concept_behaviorals.concept_child_competency_id', '=', 'concept_child_competencies.concept_child_competency_id');
		})
		->leftJoin('concept_contexts', function ($join){
			$join->on('concept_child_competencies.concept_child_competency_id', '=', 'concept_contexts.concept_child_competency_id');
		})
		->leftJoin('concept_competencies', function ($join){
			$join->on('concept_competencies.competency_id', '=', 'concept_child_competencies.concept_competencies_id');
		})
		->leftJoin(DB::raw('(SELECT employee_rate, behavioral_id, supervisor_rate, employee_is_submitted, supervisor_is_submitted FROM employee_assessments WHERE
			(employee_assessments.employee_id = '.$id.'
			OR employee_assessments.employee_id IS NULL) AND YEAR(employee_assessments.updated_at) = YEAR(CURDATE())) AS eas'), function($join){

			$join->on('eas.behavioral_id', '=', 'concept_behaviorals.behavioral_id');
		})
		->whereIn('concept_competencies.competency_id', $array1)
		->whereIn('concept_behaviorals.behavioral_id', $array2)
		->select(DB::raw('CAST(ANY_VALUE(assess.employeeAverage) AS DECIMAL (10 , 2 )) AS employeeAverage,
			CAST(ANY_VALUE(assess.supervisorAverage) AS DECIMAL (10 , 2 )) AS supervisorAverage,
			CAST(ANY_VALUE((employeeAverage * .30) + (supervisorAverage * .70))
			AS DECIMAL (10 , 2 )) AS totalAverage,
			concept_child_competencies.concept_child_competency_id,
			concept_competencies.competency_id,
			concept_competencies.competency_name,
			concept_child_competencies.name,
			ANY_VALUE(concept_behaviorals.behavioral_id) AS behavioral_id,
			ANY_VALUE(concept_behaviorals.behavioral_name) AS behavioral_name,
			ANY_VALUE(eas.employee_rate) AS employee_rate,
			ANY_VALUE(eas.supervisor_rate) AS supervisor_rate,
			ANY_VALUE(eas.employee_is_submitted) AS employee_submitted,
			ANY_VALUE(eas.supervisor_is_submitted) AS supervisor_submitted'))
		->groupBy('concept_behaviorals.behavioral_id')
		->get();

		$decode = json_decode($data, true);

		$employeeArrayNull = [];

		$startDate = DateTime::createFromFormat('m/d', '07/01')->format('m-d');
		$endDate = DateTime::createFromFormat('m/d', '08/31')->format('m-d');
		$todayDate = date("m-d");

		if($todayDate < $startDate || $todayDate > $endDate){
			
			if(!isset($competency['lock'])){
				$competency['locking_assessment_form'] = 1;
			}
			
		}else{

			if(!isset($competency['lock'])){
				$competency['locking_assessment_form'] = 0;
			}	
		}


		// return $decode;

		foreach ($decode as $element => $value) {

			if(!isset($competency['employee_submitted'])){
				$competency['employee_submitted'] = $value['employee_submitted'];
			}

			if(!isset($competency['supervisor_submitted'])){
				$competency['supervisor_submitted'] = $value['supervisor_submitted'];
			}

			if(!isset($competency[$value['competency_name']])){
				$competency[$value['competency_name']] = array(
					'competency_id' => $value['competency_id'],
					'competency_name' => $value['competency_name']);

			}

			if(!isset($competency[$value['competency_name']][$value['name']])){
				$competency[$value['competency_name']][$value['name']] = array(
					'child_competency_id' => $value['concept_child_competency_id'],
					'child_competency_name' => $value['name'],
					'employee_average' => $value['employeeAverage'],
					'supervisor_average' => $value['supervisorAverage'],
					'total_average' => $value['totalAverage']);
			}

			$competency[$value['competency_name']][$value['name']]['behaviorals'][] = array(
				'behavioral_id' => $value['behavioral_id'],
				'behavioral_name' => $value['behavioral_name'],
				'employee_rate' => $value['employee_rate'],
				'supervisor_rate' => $value['supervisor_rate']);
		}

		return $competency;

	}

	//REPORT

	public function getCompetencyName(){

		$competency = ConceptCompetency::all();
		return $competency;

	}

	public function getChildCompetency($id){

		$childCompetency = ConceptChildCompetency::where('concept_competencies_id', $id)->get();
		return $childCompetency;
	}
}
