<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Employee;
use App\EmployeeTrainingAttended;
use App\User;

class EmployeeTrainingAttendedController extends Controller
{
    //

	public function getEmployeeTrainingAttended(Request $request){

		if($request->headers->has('Authorization')){
			$token = explode(' ', $request->header('Authorization'));
			$user = User::where('remember_token', $token[1])->first();
			$employee = Employee::where('user_id', '=', $user->user_id)->first();

			if($user){

				$training = EmployeeTrainingAttended::where('employee_id', $employee->employee_id)->get();

				return $training;

			} else {
				return response()->json(['success' => 'false',
					'message' => 'User Not Found']);
			}

		}else{
			
			return response()->json(['success' => 'false',
				'message' => 'No User Authentication Founded']);
		}


	}

	public function saveEmployeeTrainingAttended(Request $request){

		if($request->headers->has('Authorization')){
			$token = explode(' ', $request->header('Authorization'));
			$user = User::where('remember_token', $token[1])->first();
			$employee = Employee::where('user_id', '=', $user->user_id)->first();

			if($user){

				$training = new EmployeeTrainingAttended;
				$training->employee_id = $employee->employee_id;
				$training->name_of_training = $request->name_of_training;
				$training->number_of_hours = $request->number_of_hours;
				$training->organizer = $request->organizer;
				$training->year_attended = $request->year_attended;

				$training->save();

				return response()->json([
					'success' => true]);
			} else {
				return response()->json(['success' => 'false',
					'message' => 'User Not Found']);
			}

		}else{
			
			return response()->json(['success' => 'false',
				'message' => 'No User Authentication Founded']);
		}

	}
}
