<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Employee;
use App\User;
use App\IncidentReport;

class IncidentReportController extends Controller
{
    //

	public function saveIncidentReport(Request $request){

		if($request->headers->has('Authorization')){
			$token = explode(' ', $request->header('Authorization'));
			$user = User::where('remember_token', $token[1])->first();
			$employee = Employee::where('user_id', '=', $user->user_id)->first();

			if($user){

				$incident = new IncidentReport;
				$incident->employee_id = $employee->employee_id;
				$incident->situation = $request->situation;
				$incident->task = $request->task;
				$incident->action = $request->action;
				$incident->result = $request->result;

				$incident->save();

				return response()->json([
					'success' => true]);
			} else {
				return response()->json(['success' => 'false',
					'message' => 'User Not Found']);
			}

		}else{
			
			return response()->json(['success' => 'false',
				'message' => 'No User Authentication Founded']);
		}

	}

	public function getIncidentReport(Request $request){

		if($request->headers->has('Authorization')){
			$token = explode(' ', $request->header('Authorization'));
			$user = User::where('remember_token', $token[1])->first();
			$employee = Employee::where('user_id', '=', $user->user_id)->first();

			if($user){

				$incidentReport = IncidentReport::where('employee_id', $employee->employee_id)->get();

				return $incidentReport;

			} else {
				return response()->json(['success' => 'false',
					'message' => 'User Not Found']);
			}

		}else{
			
			return response()->json(['success' => 'false',
				'message' => 'No User Authentication Founded']);
		}


	}
}
