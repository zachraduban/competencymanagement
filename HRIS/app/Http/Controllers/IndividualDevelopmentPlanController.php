<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\IndividualDevelopmentPlan;
use App\Employee;
use App\User;

class IndividualDevelopmentPlanController extends Controller
{
    //get IndividualDevelopmentPlan by employeeId
	public function getIdpOfEmployeeIdBySupervisorId(Request $request, $employee_id){

		if($request->headers->has('Authorization')){
			$token = explode(' ', $request->header('Authorization'));
			$user = User::where('remember_token', $token[1])->first();

			if($user){

				$idpData = IndividualDevelopmentPlan::where('employee_id', $employee_id)->get();

				return $this->returnFormattedJsonResultOfIdp($idpData);

			} else {
				return response()->json(['success' => 'false',
					'message' => 'User Not Found']);
			}
		}else{

			return response()->json(['success' => 'false',
				'message' => 'No User Authentication Founded']);
		}
	}

	//get IDP by user
	public function getIdpByEmployeeId(Request $request){
		if($request->headers->has('Authorization')){
			$token = explode(' ', $request->header('Authorization'));
			$user = User::where('remember_token', $token[1])->first();
			$employee = Employee::where('user_id', '=', $user->user_id)->first();

			if($user){
				
				$idpData = IndividualDevelopmentPlan::where('employee_id', $employee->employee_id)->get();

				return $this->returnFormattedJsonResultOfIdp($idpData);

			} else {
				return response()->json(['success' => 'false',
					'message' => 'User Not Found']);
			}
		}else{

			return response()->json(['success' => 'false',
				'message' => 'No User Authentication Founded']);
		}
	}

	public function saveIdp(Request $request){

		if($request->headers->has('Authorization')){
			$token = explode(' ', $request->header('Authorization'));
			$user = User::where('remember_token', $token[1])->first();
			$employee = Employee::where('user_id', '=', $user->user_id)->first();

			if($user){
				$data = $request->json()->all();

				if($request->exists('employee_id')){					
					return $this->saveIdpBySupervisor($data, $employee, $request->employee_id);
				}else{
					return $this->saveIdpByEmployeeId($data, $employee);
				}

				return response()->json(['success' => 'true',
					'message' => 'Successfully Save',
					'status' => 0]);

			} else {
				return response()->json(['success' => 'false',
					'message' => 'User Not Found']);
			}

		}else{
			
			return response()->json(['success' => 'false',
				'message' => 'No User Authentication Founded']);
		}

	}

	//re-format results from query of IDP to json
	public function returnFormattedJsonResultOfIdp($data){

		$decode = json_decode($data, true);
		$idpArray = [];

		foreach ($decode as $element => $value) {

			if(!isset($idpArray['employee_id'])){
				$idpArray['employee_id'] = $value['employee_id'];
			}

			if(!isset($idpArray['employee_submitted'])){
				$idpArray['employee_submitted'] = $value['employee_submitted'];
			}
			if(!isset($idpArray['supervisor_submitted'])){
				$idpArray['supervisor_submitted'] = $value['supervisor_submitted'];
			}
			if(!isset($idpArray['short_term_career_goals'])){
				$idpArray['short_term_career_goals'] = $value['short_term_career_goals'];
			}
			if(!isset($idpArray['long_term_career_goals'])){
				$idpArray['long_term_career_goals'] = $value['long_term_career_goals'];
			}

			$idpArray['competency_enhancement'][] = array(
				'idp_id' => $value['idp_id'],
				'target_competency' => $value['target_competency'],
				'dev_activity_needed' => $value['dev_activity_needed'],
				'outcome_desired' => $value['outcome_desired'],
				'support_needed' => $value['support_needed'],
				'amount_needed' => $value['amount_needed'],
				'intended_completion_date' => $value['intended_completion_date'],
				);

		}

		return $idpArray;
	}

	//save Idp of Employee
	public function saveIdpByEmployeeId($data, $user){
		
		foreach ($data['competency_enhancement'] as $key => $value) {

			$createdYear = IndividualDevelopmentPlan::where('employee_id', '=', $user->employee_id)->where('target_competency', $value['target_competency'])
			->whereYear('created_at', '=', date('Y'))->first();

			IndividualDevelopmentPlan::updateOrCreate(
				['employee_id' => $user->employee_id,
				'target_competency' => $value['target_competency'],
				'created_at' => $createdYear->created_at ?? null
				],
				[
				'employee_id' => $user->employee_id,
				'short_term_career_goals' => $data['short_term_career_goals'],
				'long_term_career_goals' => $data['long_term_career_goals'],
				'target_competency' => $value['target_competency'],
				'dev_activity_needed' => $value['dev_activity_needed'],
				'outcome_desired' => $value['outcome_desired'],
				'support_needed' => $value['support_needed'],
				'amount_needed' => $value['amount_needed'],
				'intended_completion_date' => $value['intended_completion_date'],
				'employee_submitted' => 1]
				);
		}

		return response()->json(['success' => 'true',
			'message' => 'Successfully Save',
			'status' => 0]);

	}

	public function saveIdpBySupervisor($data, $user, $id){
		
		foreach ($data['competency_enhancement'] as $key => $value) {

			$createdYear = IndividualDevelopmentPlan::where('employee_id', '=', $id)->where('idp_id', $value['idp_id'])
			->whereYear('created_at', '=', date('Y'))->first();

			IndividualDevelopmentPlan::updateOrCreate(
				['employee_id' => $id,
				'idp_id' => $value['idp_id'],
				'created_at' => $createdYear->created_at ?? null
				],
				[
				'employee_id' => $id,
				'short_term_career_goals' => $data['short_term_career_goals'],
				'long_term_career_goals' => $data['long_term_career_goals'],
				'target_competency' => $value['target_competency'],
				'dev_activity_needed' => $value['dev_activity_needed'],
				'outcome_desired' => $value['outcome_desired'],
				'support_needed' => $value['support_needed'],
				'amount_needed' => $value['amount_needed'],
				'intended_completion_date' => $value['intended_completion_date'],
				'supervisor_id' => $user->employee_id,
				'supervisor_submitted' => 1]
				);
		}

		return response()->json(['success' => 'true',
			'message' => 'Successfully Save',
			'status' => 0]);
	}
}
