<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserRegistration extends Controller
{
    //

	public function register(Request $request) {

		$user = new User();
		$user->username = $request->username;
		$user->password = Hash::make($request->password);
		$user->role_id = $request->role_id;

		$user->save();

	}

	
}
