<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Designation;
use App\Position;

use Illuminate\Support\Facades\DB;

class DesignationAndPositionController extends Controller
{
    //
    public function getPosition(){
    	$position = Position::
    	select(DB::raw('ANY_VALUE(position_id) AS position_id, position_name'))
    	->groupBy('position_name')
    	->get();
    	return $position;
    }

    public function getDesignationByPosition($id){
    	$designation = Designation::where('position_id', $id)->get();
    	return $designation;
    }

    public function getHRMOPosition(){
        $position = Position::
        select(DB::raw('ANY_VALUE(position_id) AS position_id, position_name'))
        ->where('position_name', 'Administrative Officer V')
        ->groupBy('position_name')
        ->get();
        return $position;
    }

    public function getHRMODesignationByPosition($id){
        $designation = Designation::where('position_id', $id)
        ->where('designation_id', 15)
        ->get();
        return $designation;
    }

}
