<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Employee;
use App\User;
use App\RegionAssignment;
use App\ProvinceAssignment;
use App\CityAssignment;
use App\Designation;
use App\Position;
use App\ConceptCompetency;
use App\ConceptContext;
use App\ConceptChildCompetency;
use App\ConceptMapping;
use App\ConceptBehavioral;
use App\EmployeeAssessment;
use App\Notification;

class ReportController extends Controller
{
    //
	public function getIndividualReport(Request $request){

		if($request->headers->has('Authorization')){

			$token = explode(' ', $request->header('Authorization'));

			$user = User::where('remember_token', $token[1])->first();

			if($user){

				$query = '';

				//admin and lga

				if($user->role_id == 1 || $user->role_id == 4){
					$query = Employee::leftJoin('designations', 'employees.designation_id', '=', 'designations.designation_id')
					->leftJoin('positions', 'employees.position_id','=', 'positions.position_id')
					->leftJoin('region_assignments', 'region_assignments.region_code', '=', 'employees.region_code')
					->leftJoin('province_assignments', 'province_assignments.province_code', '=', 'employees.province_code')
					->leftJoin('city_assignments', 'city_assignments.city_code', '=', 'employees.city_code')
					->select('employees.employee_id', 'employees.surname', 'employees.first_name', 'employees.middle_name', 'designations.designation_name', 'positions.position_name', 'city_assignments.city_name', 'province_assignments.province_name');
				}
                //HRMO
				elseif ($user->role_id == 2) {
					$employee = Employee::where('user_id', '=', $user->user_id)->first();
					$query = Employee::where('employees.region_code', $employee->region_code)
					->where('employees.designation_id', '!=', 15)
					->leftJoin('designations', 'employees.designation_id', '=', 'designations.designation_id')
					->leftJoin('positions', 'employees.position_id','=', 'positions.position_id')
					->leftJoin('region_assignments', 'region_assignments.region_code', '=', 'employees.region_code')
					->leftJoin('province_assignments', 'province_assignments.province_code', '=', 'employees.province_code')
					->leftJoin('city_assignments', 'city_assignments.city_code', '=', 'employees.city_code')
					->select('employees.employee_id', 'employees.surname', 'employees.first_name', 'employees.middle_name', 'designations.designation_name', 'positions.position_name', 'city_assignments.city_name', 'province_assignments.province_name');

				}
                //Supervisor
				elseif ($user->role_id == 3) {
					$employee = Employee::where('user_id', '=', $user->user_id)->first();
					$query = Notification::where('supervisor_id', $employee->employee_id)
					->where('is_approved', 1)
					->leftJoin('employees', 'notifications.employee_id', '=', 'employees.employee_id')
					->leftJoin('designations', 'employees.designation_id', '=', 'designations.designation_id')
					->leftJoin('positions', 'employees.position_id','=', 'positions.position_id')
					->leftJoin('region_assignments', 'region_assignments.region_code', '=', 'employees.region_code')
					->leftJoin('province_assignments', 'province_assignments.province_code', '=', 'employees.province_code')
					->leftJoin('city_assignments', 'city_assignments.city_code', '=', 'employees.city_code')
					->select('employees.employee_id', 'employees.surname', 'employees.first_name', 'employees.middle_name', 'designations.designation_name', 'positions.position_name', 'city_assignments.city_name', 'province_assignments.province_name', 'notifications.is_approved');
				}
				
				if($request->exists('region_code')){
					$query = $query->whereIn('employees.region_code', $request->region_code);
				}
				if($request->exists('province_code')){
					$query = $query->whereIn('employees.province_code', $request->province_code);
				}
				if($request->exists('designation_id')){
					$query = $query->where('employees.designation_id', $request->designation_id);
				}
				if($request->exists('position_id')){
					$query = $query->where('employees.position_id', $request->position_id);
				}
				if($request->exists('sex')){
					$query = $query->where('employees.sex', $request->sex);
				}
				if($request->exists('age')){
					if (strpos($request->age, '-') !== false) {
						$age = explode('-', $request->age);
						$query = $query->whereBetween(DB::raw('(YEAR(NOW()) - YEAR(`date_of_birth`))'), [$age[0], $age[1]]);
					}elseif (strpos($request->age, '+') !== false) {
						$age = explode('+', $request->age);
						$query = $query->where(DB::raw('(YEAR(NOW()) - YEAR(`date_of_birth`))'), '>', $age[0]);
					}
				}

				$query = $query->paginate(15);

				return $query;

			}else{

				return response()->json(['success' => 'false',
					'message' => 'User Not Found']);
			}

		}else{

			return response()->json(['success' => 'false',
				'message' => 'No User Authentication Founded']);

		}

	}

	public function getAggregateReport(Request $request){

		// return $request->region_code;

		if($request->report == 'Competency Assessment Per Region'){
			return $this->getCompetencyAssessmentReport($request);

		}elseif ($request->report == 'Top 10 Met Competencies') {
			return $this->getTopTenCompetencyMet($request);
		}elseif ($request->report == 'Top 10 Unmet Competencies') {
			return $this->getTopTenCompetencyUnmet($request);
		}
	}

	public function getCompetencyAssessmentReport(Request $request){

		if($request->headers->has('Authorization')){

			$token = explode(' ', $request->header('Authorization'));

			$user = User::where('remember_token', $token[1])->first();
			if($user->role_id == 2 || $user->role_id == 3){
				$employee = Employee::where('user_id', '=', $user->user_id)->first();
			}

			if($user){

				$query = DB::table('employee_assessments')
				->whereYear('employee_assessments.updated_at', '=', date('Y'))
				->leftJoin('concept_behaviorals', function($join){
					$join->on('employee_assessments.behavioral_id', '=', 'concept_behaviorals.behavioral_id');
				})
				->leftJoin('concept_child_competencies', function ($join){
					$join->on('concept_behaviorals.concept_child_competency_id', '=', 'concept_child_competencies.concept_child_competency_id');
				})
				->leftJoin('concept_contexts', function ($join){
					$join->on('concept_child_competencies.concept_child_competency_id', '=', 'concept_contexts.concept_child_competency_id');
				})
				->leftJoin('concept_competencies', function ($join){
					$join->on('concept_competencies.competency_id', '=', 'concept_child_competencies.concept_competencies_id');
				})
				->leftJoin('employees', function ($join){
					$join->on('employees.employee_id', '=', 'employee_assessments.employee_id');
				})
				->rightJoin('region_assignments', function ($join){
					$join->on('region_assignments.region_code', '=', 'employees.region_code');
				})
				->leftJoin('province_assignments', function ($join){
					$join->on('province_assignments.province_code', '=', 'employees.province_code');
				})
				->leftJoin('designations', function ($join){
					$join->on('designations.designation_id', '=', 'employees.designation_id');
				})
				->select(DB::raw('ANY_VALUE(region_assignments.region_code) AS region_code, ANY_VALUE(region_assignments.region_name) AS region_name,
					ANY_VALUE(province_assignments.province_code) AS province_code,
					ANY_VALUE(province_assignments.province_name) AS province_name,
					ANY_VALUE(designations.designation_name) AS designation_name,
					CAST(AVG((employee_rate * .30) + (supervisor_rate * .70))
					AS DECIMAL (10 , 2 )) AS totalAverage,
					concept_child_competencies.concept_child_competency_id,
					concept_competencies.competency_id,
					concept_competencies.competency_name,
					concept_child_competencies.name'));

				if($request->exists('competency_id')){
					$query = $query->where('concept_competencies.competency_id', $request->competency_id);
				}
				if($request->exists('child_competency_id')){
					$query = $query->where('concept_child_competencies.concept_child_competency_id', $request->child_competency_id);
				}

				if($user->role_id == 1){
					if($request->exists('region_code')){
						$query = $query->whereIn('employees.region_code', $request->region_code);
					}
				}else{
					$query = $query->where('employees.region_code', $request->region_code);
				}


				if($request->exists('province_code')){
					$query = $query->whereIn('employees.province_code', $request->province_code);
				}
				if($request->exists('designation_id')){
					$query = $query->where('employees.designation_id', $request->designation_id);
				}
				if($request->exists('position_id')){
					$query = $query->where('employees.position_id', $request->position_id);
				}

				if($request->exists('sex')){
					$query = $query->where('employees.sex', $request->sex);
				}
				if($request->exists('age')){
					if (strpos($request->age, '-') !== false) {
						$age = explode('-', $request->age);
						$query = $query->whereBetween(DB::raw('(YEAR(NOW()) - YEAR(`date_of_birth`))'), [$age[0], $age[1]]);
					}elseif (strpos($request->age, '+') !== false) {
						$age = explode('+', $request->age);
						$query = $query->where(DB::raw('(YEAR(NOW()) - YEAR(`date_of_birth`))'), '>', $age[0]);
					}
				}

				$query = $query->groupBy('region_assignments.region_code','concept_child_competencies.concept_child_competency_id')
				->orderBy('region_assignments.region_code', 'ASC')
				->get();

				// return $query;

				$assessment = [];
				$decode = json_decode($query, true);

				foreach ($decode as $key => $value) {

					// echo $key;
					// $assessment[$value['name']][] = array($value['totalAverage']);
					// array_push($assessment, $value['name']);

					// if(!isset($assessment[$value['region_name']])){
					// 	$assessment[$value['region_name']] = array(
					// 		'region_code' => $value['region_code'],
					// 		'region_name' => $value['region_name']);
					// }
					// if(!isset($assessment[$value['region_name']][$value['province_name']])){
					// 	$assessment[$value['region_name']][$value['province_name']] = array(
					// 		'province_code' => $value['province_code'],
					// 		'province_name' => $value['province_name']);
					// }
					// if(!isset($assessment[$value['region_name']][$value['province_name']][$value['competency_name']])){
					// 	$assessment[$value['competency_name']] = array(
					// 		'competency_id' => $value['competency_id'],
					// 		'competency_name' => $value['competency_name']);
					// }
					// if(isset($value['name'])){
					// if( array_key_exists($value['name'], $assessment) ) {
					// 	$assessment[$value['name']] = array_push($assessment,$value['totalAverage']);
					// } else {
					// 	$assessment[$value['name']] = array($value['totalAverage']);
					// }

					if(!isset($assessment[$value['region_name']])){
						$result[][$value['region_name']] = $value['region_name'];
					}

				}

				$result = array_reduce($decode, function(&$result, $current) {
					$result[$current['name']][] = array($current['region_name'] => $current['totalAverage']);
					return $result;
				}, []);

				return $result;

			}else{

				return response()->json(['success' => 'false',
					'message' => 'User Not Found']);
			}

		}else{

			return response()->json(['success' => 'false',
				'message' => 'No User Authentication Founded']);

		}
	}

	public function getTopTenCompetencyMet(Request $request){

		if($request->headers->has('Authorization')){
			$token = explode(' ', $request->header('Authorization'));
			$user = User::where('remember_token', $token[1])->first();
			if($user->role_id == 2 || $user->role_id == 3){
				$employee = Employee::where('user_id', '=', $user->user_id)->first();
			}

			if($user){

				$expectedLevelQuery = DB::table(DB::raw('(SELECT AVG(employee_rate) AS employeeAverage, ANY_VALUE(employee_rate) AS employee_rate, ANY_VALUE(CASE cb.level WHEN 2 THEN 2 + 1 WHEN 3 THEN 3 + 1 ELSE cb.level END) AS expected_level, AVG(supervisor_rate) AS supervisorAverage, ANY_VALUE(supervisor_rate) AS supervisor_rate, ANY_VALUE(assessment_id) AS assessment_id, ANY_VALUE(employee_id) AS employee_id, ANY_VALUE(employee_assessments.behavioral_id) AS behavioral_id FROM employee_assessments LEFT JOIN (concept_behaviorals AS cb) ON employee_assessments.behavioral_id = cb.behavioral_id JOIN (concept_child_competencies) ON concept_child_competencies.concept_child_competency_id = cb.concept_child_competency_id WHERE year(employee_assessments.updated_at) = year(curdate()) GROUP BY cb.level, concept_child_competencies.concept_child_competency_id , employee_assessments.employee_id) AS assess'))
				->whereYear('employee_assessments.updated_at', '=', date('Y'))
				->leftJoin('concept_behaviorals AS behaviorals', function ($join){
					$join->on('behaviorals.behavioral_id', '=', 'assess.behavioral_id');
				})
				->leftJoin('concept_child_competencies', function ($join){
					$join->on('behaviorals.concept_child_competency_id', '=', 'concept_child_competencies.concept_child_competency_id');
				})
				->leftJoin('concept_contexts', function ($join){
					$join->on('concept_child_competencies.concept_child_competency_id', '=', 'concept_contexts.concept_child_competency_id');
				})
				->leftJoin('concept_competencies', function ($join){
					$join->on('concept_competencies.competency_id', '=', 'concept_child_competencies.concept_competencies_id');
				})
				->leftJoin('employee_assessments', function ($join){
					$join->on('behaviorals.behavioral_id', '=', 'employee_assessments.behavioral_id');
				})
				->leftJoin('employees', function ($join){
					$join->on('employees.employee_id', '=', 'employee_assessments.employee_id');
				})
				->leftJoin('region_assignments', function ($join){
					$join->on('region_assignments.region_code', '=', 'employees.region_code');
				})
				->leftJoin('province_assignments', function ($join){
					$join->on('province_assignments.province_code', '=', 'employees.province_code');
				})
				->leftJoin('designations', function ($join){
					$join->on('designations.designation_id', '=', 'employees.designation_id');
				})
				->select(DB::raw('ANY_VALUE(concept_competencies.competency_id), ANY_VALUE(concept_competencies.competency_name), ANY_VALUE(concept_child_competencies.concept_child_competency_id) AS concept_child_competency_id, ANY_VALUE(concept_child_competencies.name), CAST(ANY_VALUE((employeeAverage * .30) + (supervisorAverage * .70)) AS DECIMAL (10 , 2 )) AS totalAverage, IF(CAST(ANY_VALUE((employeeAverage * .30) + (supervisorAverage * .70)) AS DECIMAL (10 , 2 )) > "3.00" AND CAST(ANY_VALUE((employeeAverage * .30) + (supervisorAverage * .70)) AS DECIMAL (10 , 2 )) <= "5.00", "MET", "UNMET") AS status,  ANY_VALUE(expected_level) AS expected_level, ANY_VALUE(CASE expected_level WHEN 4 THEN 4 ELSE expected_level + 1 END) AS max_level'));

				if($user->role_id == 1){
					if($request->exists('region_code')){
						$expectedLevelQuery = $expectedLevelQuery->whereIn('employees.region_code', $request->region_code);
					}
				}else{
					$expectedLevelQuery = $expectedLevelQuery->where('employees.region_code', $employee->region_code);
				}


				if($request->exists('province_code')){
					$expectedLevelQuery = $expectedLevelQuery->whereIn('employees.province_code', $request->province_code);
				}
				if($request->exists('designation_id')){
					$expectedLevelQuery = $expectedLevelQuery->where('employees.designation_id', $request->designation_id);
				}
				if($request->exists('position_id')){
					$expectedLevelQuery = $expectedLevelQuery->where('employees.position_id', $request->position_id);
				}

				if($request->exists('sex')){
					$expectedLevelQuery = $expectedLevelQuery->where('employees.sex', $request->sex);
				}
				if($request->exists('age')){
					if (strpos($request->age, '-') !== false) {
						$age = explode('-', $request->age);
						$expectedLevelQuery = $expectedLevelQuery->whereBetween(DB::raw('(YEAR(NOW()) - YEAR(`date_of_birth`))'), [$age[0], $age[1]]);
					}elseif (strpos($request->age, '+') !== false) {
						$age = explode('+', $request->age);
						$expectedLevelQuery = $expectedLevelQuery->where(DB::raw('(YEAR(NOW()) - YEAR(`date_of_birth`))'), '>', $age[0]);
					}
				}

				$expectedLevelQuery = $expectedLevelQuery->groupBy('behaviorals.concept_child_competency_id')
				->orderBy('totalAverage', 'DESC')
				->take(10)
				->get();

				$decodeExpectedLevel = json_decode($expectedLevelQuery, true);

				$minLevelArray = [];
				$expectedLevelArray = [];
				$maxLevelArray = [];

				$minLevelArrayResult = [];
				$expectedLevelArrayResult = [];
				$maxLevelArrayResult = [];

				$sortExpected = [];

				$minLevelAverageQuery;
				$expectedLevelAverageQuery;
				$maxLevelAverageQuery;

				foreach ($decodeExpectedLevel as $key => $value) {
					$expectedLevelArray[$value['concept_child_competency_id']] = $value['expected_level'];
					$maxLevelArray[$value['concept_child_competency_id']] = $value['max_level'];
				}

				// return $maxLevelArray; 

				foreach ($expectedLevelArray as $key2 => $value2) {

					$expectedLevelAverageQuery = DB::table('concept_behaviorals')
					->whereYear('employee_assessments.updated_at', '=', date('Y'))
					->join('employee_assessments', 'concept_behaviorals.behavioral_id', '=', 'employee_assessments.behavioral_id')
					->leftJoin('concept_child_competencies', 'concept_behaviorals.concept_child_competency_id', '=', 'concept_child_competencies.concept_child_competency_id')
					->leftJoin('concept_competencies', 'concept_child_competencies.concept_competencies_id', '=', 'concept_competencies.competency_id')
					->leftJoin('employees', function ($join){
						$join->on('employees.employee_id', '=', 'employee_assessments.employee_id');
					})
					->leftJoin('region_assignments', function ($join){
						$join->on('region_assignments.region_code', '=', 'employees.region_code');
					})
					->leftJoin('province_assignments', function ($join){
						$join->on('province_assignments.province_code', '=', 'employees.province_code');
					})
					->leftJoin('designations', function ($join){
						$join->on('designations.designation_id', '=', 'employees.designation_id');
					})
					->where('concept_behaviorals.level', $value2)
					->where('concept_child_competencies.concept_child_competency_id', $key2)
					->select(DB::raw('ANY_VALUE(concept_competencies.competency_name) AS competency_name, concept_child_competencies.concept_child_competency_id, concept_child_competencies.name, ANY_VALUE(concept_behaviorals.behavioral_id) AS behavioral_id, ANY_VALUE(concept_behaviorals.behavioral_name) AS behavioral_name, ANY_VALUE(concept_behaviorals.level) AS level, ANY_VALUE(employee_assessments.employee_rate) AS employee_rate, AVG(employee_assessments.employee_rate) AS total_employee_rate, CAST(AVG((employee_assessments.employee_rate * .30) + (employee_assessments.supervisor_rate * .70)) AS DECIMAL (10 , 2 )) AS expected_average, ANY_VALUE(region_assignments.region_code) AS region_code, ANY_VALUE(region_assignments.region_name) AS region_name'));

					if($user->role_id == 1){
						if($request->exists('region_code')){
							$expectedLevelAverageQuery = $expectedLevelAverageQuery->whereIn('employees.region_code', $request->region_code);
						}
					}else{
						$expectedLevelAverageQuery = $expectedLevelAverageQuery->where('employees.region_code', $employees->region_code);
					}


					if($request->exists('province_code')){
						$expectedLevelAverageQuery = $expectedLevelAverageQuery->whereIn('employees.province_code', $request->province_code);
					}
					if($request->exists('designation_id')){
						$expectedLevelAverageQuery = $expectedLevelAverageQuery->where('employees.designation_id', $request->designation_id);
					}
					if($request->exists('position_id')){
						$expectedLevelAverageQuery = $expectedLevelAverageQuery->where('employees.position_id', $request->position_id);
					}

					if($request->exists('sex')){
						$expectedLevelAverageQuery = $expectedLevelAverageQuery->where('employees.sex', $request->sex);
					}
					if($request->exists('age')){
						if (strpos($request->age, '-') !== false) {
							$age = explode('-', $request->age);
							$expectedLevelAverageQuery = $expectedLevelAverageQuery->whereBetween(DB::raw('(YEAR(NOW()) - YEAR(`date_of_birth`))'), [$age[0], $age[1]]);
						}elseif (strpos($request->age, '+') !== false) {
							$age = explode('+', $request->age);
							$expectedLevelAverageQuery = $expectedLevelAverageQuery->where(DB::raw('(YEAR(NOW()) - YEAR(`date_of_birth`))'), '>', $age[0]);
						}
					}

					$expectedLevelAverageQuery = $expectedLevelAverageQuery->groupBy('region_assignments.region_code','concept_behaviorals.concept_child_competency_id')

					->get();

					array_push($expectedLevelArrayResult, $expectedLevelAverageQuery);
					//array_push($sortExpected, $expectedLevelQuery['expected_average']);
				}

				// query for max result
				foreach ($maxLevelArray as $key3 => $value3) {

					$maxLevelAverageQuery = DB::table('concept_behaviorals')
					->whereYear('employee_assessments.updated_at', '=', date('Y'))
					->join('employee_assessments', 'concept_behaviorals.behavioral_id', '=', 'employee_assessments.behavioral_id')
					->leftJoin('concept_child_competencies', 'concept_behaviorals.concept_child_competency_id', '=', 'concept_child_competencies.concept_child_competency_id')
					->leftJoin('concept_competencies', 'concept_child_competencies.concept_competencies_id', '=', 'concept_competencies.competency_id')

					->leftJoin('employees', function ($join){
						$join->on('employees.employee_id', '=', 'employee_assessments.employee_id');
					})
					->leftJoin('region_assignments', function ($join){
						$join->on('region_assignments.region_code', '=', 'employees.region_code');
					})
					->leftJoin('province_assignments', function ($join){
						$join->on('province_assignments.province_code', '=', 'employees.province_code');
					})
					->leftJoin('designations', function ($join){
						$join->on('designations.designation_id', '=', 'employees.designation_id');
					})
					->where('concept_behaviorals.level', $value3)
					->where('concept_child_competencies.concept_child_competency_id', $key3)
					->select(DB::raw('ANY_VALUE(concept_competencies.competency_name) AS competency_name, concept_child_competencies.concept_child_competency_id, concept_child_competencies.name, ANY_VALUE(concept_behaviorals.behavioral_id) AS behavioral_id, ANY_VALUE(concept_behaviorals.behavioral_name) AS behavioral_name, ANY_VALUE(concept_behaviorals.level) AS level, ANY_VALUE(employee_assessments.employee_rate) AS employee_rate, AVG(employee_assessments.employee_rate) AS total_employee_rate, CAST(AVG((employee_assessments.employee_rate * .30) + (employee_assessments.supervisor_rate * .70)) AS DECIMAL (10 , 2 )) AS max_average,ANY_VALUE(region_assignments.region_code) AS region_code, ANY_VALUE(region_assignments.region_name) AS region_name'));

					if($user->role_id == 1){
						if($request->exists('region_code')){
							$maxLevelAverageQuery = $maxLevelAverageQuery->whereIn('employees.region_code', $request->region_code);
						}
					}else{
						$maxLevelAverageQuery = $maxLevelAverageQuery->where('employees.region_code', $employee->region_code);
					}


					if($request->exists('province_code')){
						$maxLevelAverageQuery = $maxLevelAverageQuery->whereIn('employees.province_code', $request->province_code);
					}
					if($request->exists('position_id')){
						$maxLevelAverageQuery = $maxLevelAverageQuery->where('employees.position_id', $request->position_id);
					}
					if($request->exists('designation_id')){
						$maxLevelAverageQuery = $maxLevelAverageQuery->where('employees.designation_id', $request->designation_id);
					}

					if($request->exists('sex')){
						$maxLevelAverageQuery = $maxLevelAverageQuery->where('employees.sex', $request->sex);
					}
					if($request->exists('age')){
						if (strpos($request->age, '-') !== false) {
							$age = explode('-', $request->age);
							$maxLevelAverageQuery = $maxLevelAverageQuery->whereBetween(DB::raw('(YEAR(NOW()) - YEAR(`date_of_birth`))'), [$age[0], $age[1]]);
						}elseif (strpos($request->age, '+') !== false) {
							$age = explode('+', $request->age);
							$maxLevelAverageQuery = $maxLevelAverageQuery->where(DB::raw('(YEAR(NOW()) - YEAR(`date_of_birth`))'), '>', $age[0]);
						}
					}

					$maxLevelAverageQuery = $maxLevelAverageQuery->groupBy('region_assignments.region_code','concept_behaviorals.concept_child_competency_id')
					->get();

					array_push($maxLevelArrayResult, $maxLevelAverageQuery);
				}

				// return $maxLevelArrayResult;

				$arrayResult = array_merge($minLevelArrayResult, $expectedLevelArrayResult, $maxLevelArrayResult);

				// return $arrayResult;

				$decodeArray;
				$responseArray = array();
				$responseArray2 = array();
				$exceededArray = array();
				$metArray = array();


				// Merge of two array into one key -> value pair
				foreach ($arrayResult as $key4 => $value4) {

					$decodeArray = json_decode($value4, true);

					// loop of decoded array
					foreach ($decodeArray as $key5 => $value5) {

						if(!isset($responseArray[$value5['region_name']])){
							$responseArray[$value5['region_name']] = array(
								'region_code' => $value5['region_code'],
								'region_name' => $value5['region_name']);
						}

						if (!isset($responseArray[$value5['region_name']][$value5['competency_name']])) {
							$responseArray[$value5['region_name']][$value5['competency_name']] = array('competency_name' => $value5['competency_name']);
						}


						if (!isset($responseArray[$value5['region_name']][$value5['competency_name']][$value5['name']])){
							$responseArray[$value5['region_name']][$value5['competency_name']][$value5['name']] = array('name' => $value5['name']);
						}

						// if(isset($value5['min_average'])){
						// 	$responseArray[$value5['competency_name']][$value5['child_competency_name']]['min_average'] = $value5['min_average'];
						// }

						if(isset($value5['expected_average'])){
							$responseArray[$value5['region_name']][$value5['competency_name']][$value5['name']]['expected_average'] = $value5['expected_average'];
						}

						if(isset($value5['max_average'])){
							$responseArray[$value5['region_name']][$value5['competency_name']][$value5['name']]['max_average'] = $value5['max_average'];
						}
					}
				}

				$result = [];

				// map the met, unmet and exceeded
				foreach ($responseArray as $key6 => $value6) {

					foreach ($value6 as $key7 => $value7) {

						if(is_array($value7)){
							foreach ($value7 as $key8 => $value8) {
								// return $responseArray2;

								if(is_array($value8)){

									if(!isset($responseArray2[$value6['region_name']])){
										$responseArray2[$value6['region_name']] = array(
											'region_code' => $value6['region_code'],
											'region_name' => $value6['region_name']);
									}

									if (!isset($responseArray2[$value6['region_name']][$value7['competency_name']])) {
										$responseArray2[$value6['region_name']][$value7['competency_name']] = array(
											'competency_name' => $value7['competency_name']);
									}

									if($value8['expected_average'] >= $value8['max_average']){
										if($value8['expected_average'] >= 1 && $value8['expected_average'] <= 3.00){

											// $responseArray2[$value6['region_name']][$value7['competency_name']]['UNMET'][] = array('name' => $value9['name'], 'average' => $value9['expected_average']);

									// echo " UNMET ";
										}else{

											$responseArray2[$value6['region_name']][$value7['competency_name']]['MET'][] = array('name' => $value8['name'],'average' => $value8['expected_average']);

									// echo " MET ";
										}

									}else{

										if($value8['max_average'] >= 1 && $value8['max_average'] <= 3.00){

											// $responseArray2[$value6['region_name']][$value7['competency_name']]['UNMET'][] = array('name' => $value8['name'],'average' => $value8['max_average']);

									// echo " UNMET ";
										}else{
											// $responseArray2[$value6['region_name']][$value7['competency_name']]['EXCEEDED'][] = array('name' => $value8['name'],'average' => $value8['max_average']);
										}
									}
								}
							}
						}
					}

				}


				// return $result;

					// $data = [];

				return $responseArray2;


			} else {
				return response()->json(['success' => 'false',
					'message' => 'User Not Found']);
			}

		}else{

			return response()->json(['success' => 'false',
				'message' => 'No User Authentication Founded']);
		}

	}  

	public function getTopTenCompetencyUnmet(Request $request){

		if($request->headers->has('Authorization')){
			$token = explode(' ', $request->header('Authorization'));
			$user = User::where('remember_token', $token[1])->first();
			if($user->role_id == 2 || $user->role_id == 3){
				$employee = Employee::where('user_id', '=', $user->user_id)->first();
			}

			if($user){

				$expectedLevelQuery = DB::table(DB::raw('(SELECT AVG(employee_rate) AS employeeAverage, ANY_VALUE(employee_rate) AS employee_rate, ANY_VALUE(CASE cb.level WHEN 2 THEN 2 + 1 WHEN 3 THEN 3 + 1 ELSE cb.level END) AS expected_level, AVG(supervisor_rate) AS supervisorAverage, ANY_VALUE(supervisor_rate) AS supervisor_rate, ANY_VALUE(assessment_id) AS assessment_id, ANY_VALUE(employee_id) AS employee_id, ANY_VALUE(employee_assessments.behavioral_id) AS behavioral_id FROM employee_assessments LEFT JOIN (concept_behaviorals AS cb) ON employee_assessments.behavioral_id = cb.behavioral_id JOIN (concept_child_competencies) ON concept_child_competencies.concept_child_competency_id = cb.concept_child_competency_id WHERE year(employee_assessments.updated_at) = year(curdate()) GROUP BY cb.level, concept_child_competencies.concept_child_competency_id , employee_assessments.employee_id) AS assess'))
				->whereYear('employee_assessments.updated_at', '=', date('Y'))
				->leftJoin('concept_behaviorals AS behaviorals', function ($join){
					$join->on('behaviorals.behavioral_id', '=', 'assess.behavioral_id');
				})
				->leftJoin('concept_child_competencies', function ($join){
					$join->on('behaviorals.concept_child_competency_id', '=', 'concept_child_competencies.concept_child_competency_id');
				})
				->leftJoin('concept_contexts', function ($join){
					$join->on('concept_child_competencies.concept_child_competency_id', '=', 'concept_contexts.concept_child_competency_id');
				})
				->leftJoin('concept_competencies', function ($join){
					$join->on('concept_competencies.competency_id', '=', 'concept_child_competencies.concept_competencies_id');
				})
				->leftJoin('employee_assessments', function ($join){
					$join->on('behaviorals.behavioral_id', '=', 'employee_assessments.behavioral_id');
				})
				->leftJoin('employees', function ($join){
					$join->on('employees.employee_id', '=', 'employee_assessments.employee_id');
				})
				->leftJoin('region_assignments', function ($join){
					$join->on('region_assignments.region_code', '=', 'employees.region_code');
				})
				->leftJoin('province_assignments', function ($join){
					$join->on('province_assignments.province_code', '=', 'employees.province_code');
				})
				->leftJoin('designations', function ($join){
					$join->on('designations.designation_id', '=', 'employees.designation_id');
				})
				->select(DB::raw('ANY_VALUE(concept_competencies.competency_id), ANY_VALUE(concept_competencies.competency_name), ANY_VALUE(concept_child_competencies.concept_child_competency_id) AS concept_child_competency_id, ANY_VALUE(concept_child_competencies.name), CAST(ANY_VALUE((employeeAverage * .30) + (supervisorAverage * .70)) AS DECIMAL (10 , 2 )) AS totalAverage, IF(CAST(ANY_VALUE((employeeAverage * .30) + (supervisorAverage * .70)) AS DECIMAL (10 , 2 )) > "3.00" AND CAST(ANY_VALUE((employeeAverage * .30) + (supervisorAverage * .70)) AS DECIMAL (10 , 2 )) <= "5.00", "MET", "UNMET") AS status,  ANY_VALUE(expected_level) AS expected_level, ANY_VALUE(CASE expected_level WHEN 4 THEN 4 ELSE expected_level + 1 END) AS max_level'));

				if($user->role_id == 1){
					if($request->exists('region_code')){
						$expectedLevelQuery = $expectedLevelQuery->whereIn('employees.region_code', $request->region_code);
					}
				}else{
					$expectedLevelQuery = $expectedLevelQuery->where('employees.region_code', $employee->region_code);
				}


				if($request->exists('province_code')){
					$expectedLevelQuery = $expectedLevelQuery->whereIn('employees.province_code', $request->province_code);
				}
				if($request->exists('designation_id')){
					$expectedLevelQuery = $expectedLevelQuery->where('employees.designation_id', $request->designation_id);
				}

				$expectedLevelQuery = $expectedLevelQuery->groupBy('behaviorals.concept_child_competency_id')
				->orderBy('totalAverage', 'DESC')
				->take(10)
				->get();

				// return $expectedLevelQuery;

				$decodeExpectedLevel = json_decode($expectedLevelQuery, true);

				$minLevelArray = [];
				$expectedLevelArray = [];
				$maxLevelArray = [];

				$minLevelArrayResult = [];
				$expectedLevelArrayResult = [];
				$maxLevelArrayResult = [];

				$minLevelAverageQuery;
				$expectedLevelAverageQuery;
				$maxLevelAverageQuery;

				foreach ($decodeExpectedLevel as $key => $value) {
					$expectedLevelArray[$value['concept_child_competency_id']] = $value['expected_level'];
					$maxLevelArray[$value['concept_child_competency_id']] = $value['max_level'];
				}

				// return $expectedLevelArray; 

				foreach ($expectedLevelArray as $key2 => $value2) {

					$expectedLevelAverageQuery = DB::table('concept_behaviorals')
					->whereYear('employee_assessments.updated_at', '=', date('Y'))
					->join('employee_assessments', 'concept_behaviorals.behavioral_id', '=', 'employee_assessments.behavioral_id')
					->leftJoin('concept_child_competencies', 'concept_behaviorals.concept_child_competency_id', '=', 'concept_child_competencies.concept_child_competency_id')
					->leftJoin('concept_competencies', 'concept_child_competencies.concept_competencies_id', '=', 'concept_competencies.competency_id')
					->leftJoin('employees', function ($join){
						$join->on('employees.employee_id', '=', 'employee_assessments.employee_id');
					})
					->leftJoin('region_assignments', function ($join){
						$join->on('region_assignments.region_code', '=', 'employees.region_code');
					})
					->leftJoin('province_assignments', function ($join){
						$join->on('province_assignments.province_code', '=', 'employees.province_code');
					})
					->leftJoin('designations', function ($join){
						$join->on('designations.designation_id', '=', 'employees.designation_id');
					})
					->where('concept_behaviorals.level', $value2)
					->where('concept_child_competencies.concept_child_competency_id', $key2)
					->select(DB::raw('ANY_VALUE(concept_competencies.competency_name) AS competency_name, concept_child_competencies.concept_child_competency_id, concept_child_competencies.name, ANY_VALUE(concept_behaviorals.behavioral_id) AS behavioral_id, ANY_VALUE(concept_behaviorals.behavioral_name) AS behavioral_name, ANY_VALUE(concept_behaviorals.level) AS level, ANY_VALUE(employee_assessments.employee_rate) AS employee_rate, AVG(employee_assessments.employee_rate) AS total_employee_rate, CAST(AVG((employee_assessments.employee_rate * .30) + (employee_assessments.supervisor_rate * .70)) AS DECIMAL (10 , 2 )) AS expected_average, ANY_VALUE(region_assignments.region_code) AS region_code, ANY_VALUE(region_assignments.region_name) AS region_name'));

					if($user->role_id == 1){
						if($request->exists('region_code')){
							$expectedLevelAverageQuery = $expectedLevelAverageQuery->whereIn('employees.region_code', $request->region_code);
						}
					}else{
						$expectedLevelAverageQuery = $expectedLevelAverageQuery->where('employees.region_code', $employee->region_code);
					}


					if($request->exists('province_code')){
						$expectedLevelAverageQuery = $expectedLevelAverageQuery->whereIn('employees.province_code', $request->province_code);
					}
					if($request->exists('designation_id')){
						$expectedLevelAverageQuery = $expectedLevelAverageQuery->where('employees.designation_id', $request->designation_id);
					}

					$expectedLevelAverageQuery = $expectedLevelAverageQuery->groupBy('region_assignments.region_code','concept_behaviorals.concept_child_competency_id')

					->get();

					array_push($expectedLevelArrayResult, $expectedLevelAverageQuery);
				}

				// return $expectedLevelAverageQuery;

				// return $expectedLevelArrayResult;

				// query for max result
				foreach ($maxLevelArray as $key3 => $value3) {

					$maxLevelAverageQuery = DB::table('concept_behaviorals')
					->whereYear('employee_assessments.updated_at', '=', date('Y'))
					->join('employee_assessments', 'concept_behaviorals.behavioral_id', '=', 'employee_assessments.behavioral_id')
					->leftJoin('concept_child_competencies', 'concept_behaviorals.concept_child_competency_id', '=', 'concept_child_competencies.concept_child_competency_id')
					->leftJoin('concept_competencies', 'concept_child_competencies.concept_competencies_id', '=', 'concept_competencies.competency_id')

					->leftJoin('employees', function ($join){
						$join->on('employees.employee_id', '=', 'employee_assessments.employee_id');
					})
					->leftJoin('region_assignments', function ($join){
						$join->on('region_assignments.region_code', '=', 'employees.region_code');
					})
					->leftJoin('province_assignments', function ($join){
						$join->on('province_assignments.province_code', '=', 'employees.province_code');
					})
					->leftJoin('designations', function ($join){
						$join->on('designations.designation_id', '=', 'employees.designation_id');
					})
					->where('concept_behaviorals.level', $value3)
					->where('concept_child_competencies.concept_child_competency_id', $key3)
					->select(DB::raw('ANY_VALUE(concept_competencies.competency_name) AS competency_name, concept_child_competencies.concept_child_competency_id, concept_child_competencies.name, ANY_VALUE(concept_behaviorals.behavioral_id) AS behavioral_id, ANY_VALUE(concept_behaviorals.behavioral_name) AS behavioral_name, ANY_VALUE(concept_behaviorals.level) AS level, ANY_VALUE(employee_assessments.employee_rate) AS employee_rate, AVG(employee_assessments.employee_rate) AS total_employee_rate, CAST(AVG((employee_assessments.employee_rate * .30) + (employee_assessments.supervisor_rate * .70)) AS DECIMAL (10 , 2 )) AS max_average,ANY_VALUE(region_assignments.region_code) AS region_code, ANY_VALUE(region_assignments.region_name) AS region_name'));

					if($user->role_id == 1){
						if($request->exists('region_code')){
							$maxLevelAverageQuery = $maxLevelAverageQuery->whereIn('employees.region_code', $request->region_code);
						}
					}else{
						$maxLevelAverageQuery = $maxLevelAverageQuery->where('employees.region_code', $employee->region_code);
					}


					if($request->exists('province_code')){
						$maxLevelAverageQuery = $maxLevelAverageQuery->whereIn('employees.province_code', $request->province_code);
					}
					if($request->exists('designation_id')){
						$maxLevelAverageQuery = $maxLevelAverageQuery->where('employees.designation_id', $request->designation_id);
					}

					$maxLevelAverageQuery = $maxLevelAverageQuery->groupBy('region_assignments.region_code','concept_behaviorals.concept_child_competency_id')
					->get();

					array_push($maxLevelArrayResult, $maxLevelAverageQuery);
				}

				// return $maxLevelArrayResult;

				$arrayResult = array_merge($minLevelArrayResult, $expectedLevelArrayResult, $maxLevelArrayResult);

				// return $arrayResult;

				$decodeArray;
				$responseArray = array();
				$responseArray2 = array();
				$exceededArray = array();
				$metArray = array();


				// Merge of two array into one key -> value pair
				foreach ($arrayResult as $key4 => $value4) {

					$decodeArray = json_decode($value4, true);

					// loop of decoded array
					foreach ($decodeArray as $key5 => $value5) {

						if(!isset($responseArray[$value5['region_name']])){
							$responseArray[$value5['region_name']] = array(
								'region_code' => $value5['region_code'],
								'region_name' => $value5['region_name']);
						}

						if (!isset($responseArray[$value5['region_name']][$value5['competency_name']])) {
							$responseArray[$value5['region_name']][$value5['competency_name']] = array('competency_name' => $value5['competency_name']);
						}


						if (!isset($responseArray[$value5['region_name']][$value5['competency_name']][$value5['name']])){
							$responseArray[$value5['region_name']][$value5['competency_name']][$value5['name']] = array('name' => $value5['name']);
						}

						// if(isset($value5['min_average'])){
						// 	$responseArray[$value5['competency_name']][$value5['child_competency_name']]['min_average'] = $value5['min_average'];
						// }

						if(isset($value5['expected_average'])){
							$responseArray[$value5['region_name']][$value5['competency_name']][$value5['name']]['expected_average'] = $value5['expected_average'];
						}

						if(isset($value5['max_average'])){
							$responseArray[$value5['region_name']][$value5['competency_name']][$value5['name']]['max_average'] = $value5['max_average'];
						}

					}
				}

				// return $responseArray;

				// map the met, unmet and exceeded
				foreach ($responseArray as $key6 => $value6) {

					foreach ($value6 as $key7 => $value7) {

						if(is_array($value7)){
							foreach ($value7 as $key8 => $value8) {
								// return $responseArray2;

								if(is_array($value8)){

									if(!isset($responseArray2[$value6['region_name']])){
										$responseArray2[$value6['region_name']] = array(
											'region_code' => $value6['region_code'],
											'region_name' => $value6['region_name']);
									}

									if (!isset($responseArray2[$value6['region_name']][$value7['competency_name']])) {
										$responseArray2[$value6['region_name']][$value7['competency_name']] = array(
											'competency_name' => $value7['competency_name']);
									}

									if($value8['expected_average'] >= $value8['max_average']){
										if($value8['expected_average'] >= 1 && $value8['expected_average'] <= 3.00){

											$responseArray2[$value6['region_name']][$value7['competency_name']]['UNMET'][] = array('name' => $value8['name'], 'average' => $value8['expected_average']);

									// echo " UNMET ";
										}else{

											// $responseArray2[$value6['region_name']][$value7['competency_name']]['MET'][] = array('name' => $value8['name'],'average' => $value8['expected_average']);

									// echo " MET ";
										}

									}else{

										if($value8['max_average'] >= 1 && $value8['max_average'] <= 3.00){

											$responseArray2[$value6['region_name']][$value7['competency_name']]['UNMET'][] = array('name' => $value8['name'],'average' => $value8['max_average']);

									// echo " UNMET ";
										}else{
											// $responseArray2[$value6['region_name']][$value7['competency_name']]['EXCEEDED'][] = array('name' => $value8['name'],'average' => $value8['max_average']);
										}
									}
								}
							}
						}
					}

				}

				return $responseArray2;


			} else {
				return response()->json(['success' => 'false',
					'message' => 'User Not Found']);
			}

		}else{

			return response()->json(['success' => 'false',
				'message' => 'No User Authentication Founded']);
		}

	}  
}
