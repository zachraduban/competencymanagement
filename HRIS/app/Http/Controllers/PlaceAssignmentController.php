<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\RegionAssignment;
use App\ProvinceAssignment;
use App\CityAssignment;
use App\Employee;
use App\User;
use App\Role;
use App\Notification;

class PlaceAssignmentController extends Controller
{
    //

    public function getRegionPlace(){
    	$regionAssignment = RegionAssignment::all();

    	return $regionAssignment;

    }

    public function getProvinceByRegion($regionCode){
    	$province = ProvinceAssignment::where('region_code', $regionCode)->get();
    	return $province;
    }

    public function getAllProvince($provinceName){
        $province = ProvinceAssignment::where('province_name', 'like', '%'.$provinceName.'%')->get();
        return $province;
    }

    public function getCityByProvince($cityCode){
    	$city = CityAssignment::where('province_code', $cityCode)->get();
    	return $city;
    }

    public function getAllCity($cityName){
        $city = CityAssignment::where('city_name', 'like', '%'.$cityName.'%')->get();
        return $city;
    }


    /**
    Report Filter
    */

    public function getRegionFilter(Request $request){

        if($request->headers->has('Authorization')){

            $token = explode(' ', $request->header('Authorization'));
            $user = User::where('remember_token', $token[1])->first();
            $employee = Employee::where('user_id', '=', $user->user_id)->first();

            if($user){

                $query = "";

                //admin and lga

                if($user->role_id == 1 || $user->role_id == 4){
                    $query = RegionAssignment::all();
                }
                //HRMO
                elseif ($user->role_id == 2) {
                    $query = Employee::where('employees.region_code', $employee->region_code)
                    ->leftJoin('region_assignments', 'region_assignments.region_code', '=', 'employees.region_code')
                    ->select('region_assignments.region_id', 'region_assignments.region_code', 'region_assignments.region_name')
                    ->groupBy('region_assignments.region_id')
                    ->get();

                }
                //Supervisor
                elseif ($user->role_id == 3) {
                    $query = Notification::where('supervisor_id', $employee->employee_id)
                    ->where('is_approved', 1)
                    ->leftJoin('employees', 'notifications.employee_id', '=', 'employees.employee_id')
                    ->leftJoin('region_assignments', 'region_assignments.region_code', '=', 'employees.region_code')
                    ->select('region_assignments.region_id', 'region_assignments.region_code', 'region_assignments.region_name')
                    ->groupBy('region_assignments.region_id')
                    ->get();
                }

                return $query;

            }else{

                return response()->json(['success' => 'false',
                    'message' => 'User Not Found']);

            }
        }else{

            return response()->json(['success' => 'false',
                'message' => 'No User Authentication Founded']);

        }

    }
}