<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Employee;
use App\User;
use App\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class LogInEmployeeController extends Controller
{
    //

	public function __construct(){
		$this->token = uniqid(base64_encode(str_random(60)));
	}

	public function logInUser(Request $request){

		$user = User::where('username', $request->username)->first();
		$employee = Employee::where('user_id', '=', $user->user_id)
		->leftJoin('designations', 'employees.designation_id', '=', 'designations.designation_id')
		->leftJoin('positions', 'employees.position_id','=', 'positions.position_id')
		->leftJoin('region_assignments', 'employees.region_code', '=', 'region_assignments.region_code')
		->leftJoin('province_assignments', 'province_assignments.province_code', '=', 'employees.province_code')
		->leftJoin('city_assignments', 'city_assignments.city_code', '=', 'employees.city_code')
		->first();
		$supervisor = '';

		if($user){
			if (Hash::check($request->password, $user->password)) {
				$token_array = ['remember_token' => $this->token];
				$logInEmployee = User::where('username', $user->username)->update($token_array);
				$role = Role::where('role_id', $user->role_id)->first();

				if($employee){
					$supervisor = Employee::where('employee_id', '=', $employee->supervisor)
					->leftJoin('designations', 'employees.designation_id', '=', 'designations.designation_id')
					->leftJoin('positions', 'employees.position_id','=', 'positions.position_id')
					->leftJoin('region_assignments', 'employees.region_code', '=', 'region_assignments.region_code')
					->leftJoin('province_assignments', 'province_assignments.province_code', '=', 'employees.province_code')
					->leftJoin('city_assignments', 'city_assignments.city_code', '=', 'employees.city_code')
					->first();
				}

				return response()->json([
					'success' => 'true',
					'employee' => $employee,
					'supervisor' => $supervisor,
					'user' => $user,
					'role' => $role,
					'api_token' => $this->token
					]);
			}else{
				return response()->json(['success' => 'false',
					'message' => 'Invalid username or password']);
			}
		}else{
			// return redirect('pnofile.html');
			return response()->json(['success' => 'false',
				'message' => 'User Not Found']);
		}
	}

	public function logOutUser(Request $request){

		if($request->headers->has('Authorization')){

			$token = explode(' ', $request->header('Authorization'));
			$user = User::where('remember_token', $token[1])->first();
			$employee = Employee::where('user_id', '=', $user->user_id)->first();

			if($user){

				$token_array = ['remember_token' => null];

				$logOutEmployee = Employee::where('employee_id', $employee->employee_id)->update($token_array);

				if($logOutEmployee){

					return response()->json([
						'success' => 'true',
						'message' => 'User Logged Out']);

				}

			}else{

				return response()->json(['success' => 'false',
					'message' => 'User Not Found']);

			}
		}else{

			return response()->json(['success' => 'false',
				'message' => 'No User Authentication Founded']);

		}

	}

	public function forgotPassword(Request $request){

		$employee = Employee::where('email', $request->email)->first();

		if($employee){

			$random = str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890');
			$password = substr($random, 0, 10);

			if($password != null){

				$to = "no-reply@gmail.com";
				$subject = "User Credentials";
				$headers = "From: no-reply@gmail.com" . "\r\n";
				$headers .= "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

				$message = "
				<html>
				<head>
					<title>HTML email</title>
				</head>
				<body>
					<p>User Credentials</p>
					<table>
						<tr>
							<th>New Password</th>
						</tr>
						<tr>
							<td>".$password."</td>
						</tr>
					</table>
					<p>Kindly change your password</p>
				</body>
				</html>
				";

				mail($request->email,"User Registration",$message, $headers);

            //hash the password
				$password_array = ['password' => Hash::make($password)];

				$updatedPasswordEmployee = User::where('user_id', $employee->user_id)
				->update($password_array);

				return response()->json([
					'success' => 'true',
					'message' => 'Saved new password'

					]);

			}

		}else{

			return response()->json([
				'success' => 'false',
				'message' => 'Email address does not exist'

				]);

		}
	}
}
