<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Employee;
use App\User;
use App\Designation;
use App\Notification;

class PersonnelListController extends Controller
{
    //get employee under supervisor for approval
	public function getEmployeePerSupervisor(Request $request){

		if($request->headers->has('Authorization')){

			$token = explode(' ', $request->header('Authorization'));

			$user = User::where('remember_token', $token[1])->first();
			$employee = Employee::where('user_id', '=', $user->user_id)->first();


			if($user){

				$query = Notification::where('supervisor_id', $employee->employee_id)
				->where('is_approved', 0)
				->leftJoin('employees', 'notifications.employee_id', '=', 'employees.employee_id')
				->leftJoin('designations', 'employees.designation_id', '=', 'designations.designation_id')
				->leftJoin('positions', 'employees.position_id','=', 'positions.position_id')
				->leftJoin('province_assignments', 'province_assignments.province_code', '=', 'employees.province_code')
				->leftJoin('city_assignments', 'city_assignments.city_code', '=', 'employees.city_code')
				->select('employees.employee_id', 'employees.surname', 'employees.first_name', 'employees.middle_name', 'designations.designation_name', 'positions.position_name', 'city_assignments.city_name', 'province_assignments.province_name', 'notifications.is_approved')
				->paginate(15);

				return response()->json([
					'count' => count($query),
					$query]);

			}else{

				return response()->json(['success' => 'false',
					'message' => 'User Not Found']);
			}

		}else{

			return response()->json(['success' => 'false',
				'message' => 'No User Authentication Founded']);

		}
	}

	//get Approved Employee of Supervisor
	public function getApprovedEmployeeBySupervisor(Request $request){

		if($request->headers->has('Authorization')){

			$token = explode(' ', $request->header('Authorization'));

			$user = User::where('remember_token', $token[1])->first();
			$employee = Employee::where('user_id', '=', $user->user_id)->first();

			if($user){

				$query = Notification::where('supervisor_id', $employee->employee_id)
				->where('is_approved', 1)
				->leftJoin('employees', 'notifications.employee_id', '=', 'employees.employee_id')
				->leftJoin('designations', 'employees.designation_id', '=', 'designations.designation_id')
				->leftJoin('positions', 'employees.position_id','=', 'positions.position_id')
				->leftJoin('province_assignments', 'province_assignments.province_code', '=', 'employees.province_code')
				->leftJoin('city_assignments', 'city_assignments.city_code', '=', 'employees.city_code')
				->select('employees.employee_id', 'employees.surname', 'employees.first_name', 'employees.middle_name', 'designations.designation_name', 'positions.position_name', 'city_assignments.city_name', 'province_assignments.province_name', 'notifications.is_approved')
				->paginate(15);

				return $query;

			}else{

				return response()->json(['success' => 'false',
					'message' => 'User Not Found']);
			}

		}else{

			return response()->json(['success' => 'false',
				'message' => 'No User Authentication Founded']);

		}
	}

	//approve the employee requested for supervisor
	public function approveEmployeeRequest(Request $request, $id){

		if($request->headers->has('Authorization')){

			$token = explode(' ', $request->header('Authorization'));

			$user = User::where('remember_token', $token[1])->first();
			$employee = Employee::where('user_id', '=', $user->user_id)->first();

			if($user){
				if(strcasecmp($request->status, 'Confirm') == 0){
					$status = ['is_approved' => 1];

					$logInEmployee = Notification::where('employee_id', $id)
					->where('supervisor_id', $employee->employee_id)
					->update($status);

					Employee::where('employee_id', $id)
					->update(['supervisor'=> $employee->employee_id]);

				}else if(strcasecmp($request->status, 'Delete') == 0){

					$deletedRows = Notification::where('employee_id', $id)->delete();
				}

				return response()->json([
					'success' => 'true']);

			}else{

				return response()->json(['success' => 'false',
					'message' => 'User Not Found']);
			}

		}else{

			return response()->json(['success' => 'false',
				'message' => 'No User Authentication Founded']);

		}
	} 


	public function getEmployeeAverageLevel(Request $request, $id){

		if($request->headers->has('Authorization')){
			$token = explode(' ', $request->header('Authorization'));
			$user = User::where('remember_token', $token[1])->first();

			if($user){

				$expectedLevelQuery = DB::table(DB::raw('(SELECT AVG(employee_rate) AS employeeAverage, ANY_VALUE(employee_rate) AS employee_rate, ANY_VALUE(CASE cb.level WHEN 2 THEN 2 + 1 WHEN 3 THEN 3 + 1 ELSE cb.level END) AS expected_level, AVG(supervisor_rate) AS supervisorAverage, ANY_VALUE(supervisor_rate) AS supervisor_rate, ANY_VALUE(assessment_id) AS assessment_id, ANY_VALUE(employee_id) AS employee_id, ANY_VALUE(employee_assessments.behavioral_id) AS behavioral_id FROM employee_assessments LEFT JOIN (concept_behaviorals AS cb) ON employee_assessments.behavioral_id = cb.behavioral_id JOIN (concept_child_competencies) ON concept_child_competencies.concept_child_competency_id = cb.concept_child_competency_id WHERE employee_assessments.employee_id = '.$id. ' AND year(employee_assessments.updated_at) = year(curdate()) GROUP BY cb.level, concept_child_competencies.concept_child_competency_id , employee_assessments.employee_id) AS assess'))
				->whereYear('employee_assessments.updated_at', '=', date('Y'))
				->leftJoin('concept_behaviorals AS behaviorals', function ($join){
					$join->on('behaviorals.behavioral_id', '=', 'assess.behavioral_id');
				})
				->leftJoin('concept_child_competencies', function ($join){
					$join->on('behaviorals.concept_child_competency_id', '=', 'concept_child_competencies.concept_child_competency_id');
				})
				->leftJoin('concept_contexts', function ($join){
					$join->on('concept_child_competencies.concept_child_competency_id', '=', 'concept_contexts.concept_child_competency_id');
				})
				->leftJoin('concept_competencies', function ($join){
					$join->on('concept_competencies.competency_id', '=', 'concept_child_competencies.concept_competencies_id');
				})
				->leftJoin('employee_assessments', function ($join){
					$join->on('behaviorals.behavioral_id', '=', 'employee_assessments.behavioral_id');
				})
				->where('employee_assessments.employee_id', $id)
				->orWhere(DB::raw('employee_assessments.employee_id'))
				->select(DB::raw('ANY_VALUE(concept_competencies.competency_id), ANY_VALUE(concept_competencies.competency_name), ANY_VALUE(concept_child_competencies.concept_child_competency_id) AS concept_child_competency_id, ANY_VALUE(concept_child_competencies.name), CAST(ANY_VALUE((employeeAverage * .30) + (supervisorAverage * .70)) AS DECIMAL (10 , 2 )) AS totalAverage, IF(CAST(ANY_VALUE((employeeAverage * .30) + (supervisorAverage * .70)) AS DECIMAL (10 , 2 )) > "3.00" AND CAST(ANY_VALUE((employeeAverage * .30) + (supervisorAverage * .70)) AS DECIMAL (10 , 2 )) <= "5.00", "MET", "UNMET") AS status,  ANY_VALUE(expected_level) AS expected_level, ANY_VALUE(CASE expected_level WHEN 4 THEN 4 ELSE expected_level + 1 END) AS max_level'))
				->groupBy('behaviorals.concept_child_competency_id')
				->get();

				// return $expectedLevelQuery;

				$decodeExpectedLevel = json_decode($expectedLevelQuery, true);
				
				$minLevelArray = [];
				$expectedLevelArray = [];
				$maxLevelArray = [];

				$minLevelArrayResult = [];
				$expectedLevelArrayResult = [];
				$maxLevelArrayResult = [];
				
				$minLevelAverageQuery;
				$expectedLevelAverageQuery;
				$maxLevelAverageQuery;

				foreach ($decodeExpectedLevel as $key => $value) {
					$expectedLevelArray[$value['concept_child_competency_id']] = $value['expected_level'];
					$maxLevelArray[$value['concept_child_competency_id']] = $value['max_level'];
				}

				// return $expectedLevelArray; 

				// query for min result
				// foreach ($minLevelArray as $keyMin => $valueMin) {

				// 	$minLevelAverageQuery = DB::table(DB::raw('(SELECT AVG(employee_rate) AS employeeAverage, ANY_VALUE(employee_rate) AS employee_rate, ANY_VALUE(level) AS expected_level, ANY_VALUE(case cb.level when 4 THEN 4 else max(cb.level) - min(cb.level) end) AS status_level, AVG(supervisor_rate) AS supervisorAverage, ANY_VALUE(supervisor_rate) AS supervisor_rate, ANY_VALUE(assessment_id) AS assessment_id, ANY_VALUE(employee_id) AS employee_id, ANY_VALUE(employee_assessments.behavioral_id) AS behavioral_id FROM employee_assessments LEFT JOIN (concept_behaviorals AS cb) ON employee_assessments.behavioral_id = cb.behavioral_id JOIN (concept_child_competencies) ON concept_child_competencies.concept_child_competency_id = cb.concept_child_competency_id WHERE employee_assessments.employee_id =' .$id. ' GROUP BY cb.level, concept_child_competencies.concept_child_competency_id , employee_assessments.employee_id) AS assess'))
				// 	->leftJoin('concept_behaviorals AS behaviorals', function ($join){
				// 		$join->on('behaviorals.behavioral_id', '=', 'assess.behavioral_id');
				// 	})
				// 	->leftJoin('concept_child_competencies', function ($join){
				// 		$join->on('behaviorals.concept_child_competency_id', '=', 'concept_child_competencies.concept_child_competency_id');
				// 	})
				// 	->leftJoin('concept_contexts', function ($join){
				// 		$join->on('concept_child_competencies.concept_child_competency_id', '=', 'concept_contexts.concept_child_competency_id');
				// 	})
				// 	->leftJoin('concept_competencies', function ($join){
				// 		$join->on('concept_competencies.competency_id', '=', 'concept_child_competencies.concept_competencies_id');
				// 	})
				// 	->leftJoin('employee_assessments', function ($join){
				// 		$join->on('behaviorals.behavioral_id', '=', 'employee_assessments.behavioral_id');
				// 	})
				// 	->where(function($query) use($id) {
				// 		$query->where('employee_assessments.employee_id', $id)
				// 		->orWhere(DB::raw('employee_assessments.employee_id'));
				// 	})

				// 	->where('behaviorals.level', $valueMin)
				// 	->select(DB::raw('ANY_VALUE(concept_competencies.competency_id) AS competency_id , ANY_VALUE(concept_competencies.competency_name) AS competency_name, ANY_VALUE(concept_child_competencies.name) AS child_competency_name, CAST(ANY_VALUE((employeeAverage * .30) + (supervisorAverage * .70)) AS DECIMAL (10 , 2 )) AS min_average, IF(CAST(ANY_VALUE((employeeAverage * .30) + (supervisorAverage * .70)) AS DECIMAL (10 , 2 )) > "3.00" AND CAST(ANY_VALUE((employeeAverage * .30) + (supervisorAverage * .70)) AS DECIMAL (10 , 2 )) <= "5.00", "MET", "UNMET") AS status, ANY_VALUE(case behaviorals.level when 4 THEN 4 else max(behaviorals.level) - min(behaviorals.level) end) AS expected_level, max(behaviorals.level) AS max_level, min(behaviorals.level) AS min_level, ANY_VALUE(behaviorals.level) AS level_value'))
				// 	->groupBy('behaviorals.concept_child_competency_id')
				// 	->offset($keyMin)
				// 	->limit(1)
				// 	->get();

				// 	array_push($minLevelArrayResult, $minLevelAverageQuery);
				// }

				// return $minLevelArrayResult;

				foreach ($expectedLevelArray as $key2 => $value2) {

					$expectedLevelAverageQuery = DB::table('concept_behaviorals')
					->whereYear('employee_assessments.updated_at', '=', date('Y'))
					->join('employee_assessments', 'concept_behaviorals.behavioral_id', '=', 'employee_assessments.behavioral_id')
					->leftJoin('concept_child_competencies', 'concept_behaviorals.concept_child_competency_id', '=', 'concept_child_competencies.concept_child_competency_id')
					->leftJoin('concept_competencies', 'concept_child_competencies.concept_competencies_id', '=', 'concept_competencies.competency_id')
					->where(function($query) use($id) {
						$query->where('employee_assessments.employee_id', $id)
						->orWhere(DB::raw('employee_assessments.employee_id'));
					})

					->where('concept_behaviorals.level', $value2)
					->where('concept_child_competencies.concept_child_competency_id', $key2)
					->select(DB::raw('ANY_VALUE(concept_competencies.competency_name) AS competency_name, concept_child_competencies.concept_child_competency_id, concept_child_competencies.name, ANY_VALUE(concept_behaviorals.behavioral_id) AS behavioral_id, ANY_VALUE(concept_behaviorals.behavioral_name) AS behavioral_name, ANY_VALUE(concept_behaviorals.level) AS level, ANY_VALUE(employee_assessments.employee_rate) AS employee_rate, AVG(employee_assessments.employee_rate) AS total_employee_rate, CAST(AVG((employee_assessments.employee_rate * .30) + (employee_assessments.supervisor_rate * .70)) AS DECIMAL (10 , 2 )) AS expected_average'))
					->groupBy('concept_behaviorals.concept_child_competency_id')
					->get();
					
					array_push($expectedLevelArrayResult, $expectedLevelAverageQuery);
				}

				// return $expectedLevelArrayResult;

				// query for max result
				foreach ($maxLevelArray as $key3 => $value3) {
					
					$maxLevelAverageQuery = DB::table('concept_behaviorals')
					->whereYear('employee_assessments.updated_at', '=', date('Y'))
					->join('employee_assessments', 'concept_behaviorals.behavioral_id', '=', 'employee_assessments.behavioral_id')
					->leftJoin('concept_child_competencies', 'concept_behaviorals.concept_child_competency_id', '=', 'concept_child_competencies.concept_child_competency_id')
					->leftJoin('concept_competencies', 'concept_child_competencies.concept_competencies_id', '=', 'concept_competencies.competency_id')
					->where(function($query) use($id) {
						$query->where('employee_assessments.employee_id', $id)
						->orWhere(DB::raw('employee_assessments.employee_id'));
					})

					->where('concept_behaviorals.level', $value3)
					->where('concept_child_competencies.concept_child_competency_id', $key3)
					->select(DB::raw('ANY_VALUE(concept_competencies.competency_name) AS competency_name, concept_child_competencies.concept_child_competency_id, concept_child_competencies.name, ANY_VALUE(concept_behaviorals.behavioral_id) AS behavioral_id, ANY_VALUE(concept_behaviorals.behavioral_name) AS behavioral_name, ANY_VALUE(concept_behaviorals.level) AS level, ANY_VALUE(employee_assessments.employee_rate) AS employee_rate, AVG(employee_assessments.employee_rate) AS total_employee_rate, CAST(AVG((employee_assessments.employee_rate * .30) + (employee_assessments.supervisor_rate * .70)) AS DECIMAL (10 , 2 )) AS max_average'))
					->groupBy('concept_behaviorals.concept_child_competency_id')
					->get();

					array_push($maxLevelArrayResult, $maxLevelAverageQuery);
				}

				// return $maxLevelArrayResult;

				$arrayResult = array_merge($minLevelArrayResult, $expectedLevelArrayResult, $maxLevelArrayResult);

				// return $arrayResult;

				$decodeArray;
				$responseArray = array();
				$responseArray2 = array();
				$exceededArray = array();
				$metArray = array();


				// Merge of two array into one key -> value pair
				foreach ($arrayResult as $key4 => $value4) {
					
					$decodeArray = json_decode($value4, true);

					// loop of decoded array
					foreach ($decodeArray as $key5 => $value5) {
						
						if (!isset($responseArray[$value5['competency_name']])) {
							$responseArray[$value5['competency_name']] = array('competency_name' => $value5['competency_name']);
						}


						if (!isset($responseArray[$value5['competency_name']][$value5['name']])){
							$responseArray[$value5['competency_name']][$value5['name']] = array('name' => $value5['name']);
						}

						// if(isset($value5['min_average'])){
						// 	$responseArray[$value5['competency_name']][$value5['child_competency_name']]['min_average'] = $value5['min_average'];
						// }

						if(isset($value5['expected_average'])){
							$responseArray[$value5['competency_name']][$value5['name']]['expected_average'] = $value5['expected_average'];
						}

						if(isset($value5['max_average'])){
							$responseArray[$value5['competency_name']][$value5['name']]['max_average'] = $value5['max_average'];
						}

					}
				}

				// map the met, unmet and exceeded
				foreach ($responseArray as $key6 => $value6) {

					foreach ($value6 as $key7 => $value7) {

						if(is_array($value7)){
							if (!isset($responseArray2[$value6['competency_name']])) {
								$responseArray2[$value6['competency_name']] = array(
									'competency_name' => $value6['competency_name']);
							}

							// if($value7['min_average'] > $value7['expected_average']){
							// 	echo " UNMET ";
							// 	if (!isset($responseArray2[$value6['competency_name']])) {
							// 		$responseArray2[$value6['competency_name']]['UNMET'][$value7['child_competency_name']] = array(
							// 			'child_competency_name' => $value7['child_competency_name']);
							// 	}

							// 	// echo $key8[$value8];

							// 	// var_dump($value8);

							// 	// var_dump($value8['min_average']);

							// 	// if($'min_average'] > $value6['expected_average']){
							// 	// 	if (!isset($responseArray2[$value6['competency_name']])) {
							// 	// 		$responseArray2[$value6['competency_name']]['UNMET'][$value6['child_competency_name']] = array(
							// 	// 			'competency_name' => $value6['competency_name']);
							// 	// 	}
							// 	// }
							// }else{

							if($value7['expected_average'] >= $value7['max_average']){
								if($value7['expected_average'] >= 1 && $value7['expected_average'] <= 3.00){
									
									$responseArray2[$value6['competency_name']]['UNMET'][] = array('name' => $value7['name']);
									
									// echo " UNMET ";
								}else{

									$responseArray2[$value6['competency_name']]['MET'][] = array('name' => $value7['name']);

									// echo " MET ";
								}

							}else{

								if($value7['max_average'] >= 1 && $value7['max_average'] <= 3.00){
									
									$responseArray2[$value6['competency_name']]['UNMET'][] = array('name' => $value7['name']);
									
									// echo " UNMET ";
								}else{
									$responseArray2[$value6['competency_name']]['EXCEEDED'][] = array('name' => $value7['name']);
								}
							}

						}
					}

				}

				return $responseArray2;

			} else {
				return response()->json(['success' => 'false',
					'message' => 'User Not Found']);
			}

		}else{
			
			return response()->json(['success' => 'false',
				'message' => 'No User Authentication Founded']);
		}

	}  
}
