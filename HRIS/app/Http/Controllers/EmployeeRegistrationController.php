<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;

use App\Employee;
use App\EmployeeAddress;
use App\EmployeePlaceMap;
use App\User;
use App\RegionAssignment;
use App\ProvinceAssignment;
use App\CityAssignment;
use App\Designation;
use App\Notification;
use App\TempPlaceOfAssignment;

class EmployeeRegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $employeeEmail = Employee::where('email', $request->email)->first();

        $employee = new Employee;
        $employee->surname = $request->surname;
        $employee->first_name = $request->firstName;
        $employee->middle_name = $request->middleName;
        $employee->extension = $request->extension;
        $employee->date_of_birth = date('Y/m/d', strtotime($request->dateOfBirth));
        $employee->place_of_birth = $request->placeOfBirth;
        $employee->sex = $request->sex;
        $employee->civil_status = $request->civilStatus;
        $employee->avatar = $request->avatar;
        $employee->street = $request->street;
        $employee->city_or_municipality = $request->city;
        $employee->barangay = $request->barangay;
        $employee->zipcode = $request->zipcode;
        $employee->tel_no = $request->telNo;
        $employee->mobile_no = $request->mobileNo;
        if($employeeEmail){

            return response()->json([
                'success' => false,
                'message' => 'Email Address already exist']);
        }else{
            $employee->email = $request->email;
        }
        $employee->region_code = $request->regionCode;
        $employee->province_code = $request->provinceCode;
        $employee->city_code = $request->cityCode;
        $employee->designation_id = $request->designation_id;
        $employee->position_id = $request->position_id;
        $employee->hrmo_is_approved = 0;
        $employee->save();

        $notification = new Notification;
        $notification->employee_id = $employee->employee_id;
        $notification->supervisor_id = $request->supervisor;
        $notification->is_approved = 0;

        $notification->save();

        return response()->json([
            'success' => 'true'
            ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Get the Supervisors
     * 
     * @return crm/getSupervisor/designation
     */
    public function getSupervisorDesignation(){

        $designationSupervisor = DB::table('designations')
        ->leftjoin('positions', function ($join){

            $join->on('designations.designation_id', '=', 'positions.designation_id');

        })
        
        ->select('designations.designation_name', 'positions.position_name')
        ->where('designations.designation_name', 'like', '%Regional Director%')
        ->orWhere('designations.designation_name', 'like', '%Assistant Regional Director%')
        ->orWhere('designations.designation_name', 'like', '%Cluster Head%')
        ->orWhere('designations.designation_name', 'like', '%Provincial Director%')
        ->orWhere('designations.designation_name', 'like', '%Chief Administrative Officer%')
        ->get();

        return $designationSupervisor;
    }

    public function getSupervisor($regionCode = null, $designation = null, $name = null){

        $designations = explode(' - ', $designation);
        $names = explode(' ', $name);
        $employee = DB::table('employees')
        ->leftjoin('designations', function ($join){

            $join->on('employees.designation_id', '=', 'designations.designation_id');


        })
        
        ->leftjoin('positions', function ($join){

            $join->on('employees.position_id', '=', 'positions.position_id');

        })
        
        ->select('employee_id', 'first_name', 'middle_name', 'surname', 'positions.position_name', 'designations.designation_name')
        ->where('region_code', $regionCode)
        ->where(function($query) use($designations){

            $query->where('designation_name', 'like', '%'.$designations[1].'%');
            $query->where(function($query) use($designations){

                $query->where('position_name', $designations[0]);

            });

        })
        
        ->where(function($query) use($names){

            $query->where('first_name', 'like', '%'.$names[0].'%');
            $query->orWhere(function($query) use($names){

                $query->where('surname', 'like', '%'.$names[0].'%');

            });

        })
        
        ->get();
        
        return $employee;
    }

    //POST
    // Update Employee Registration
    public function updateEmployeeData(Request $request){

        if($request->headers->has('Authorization')){

            $token = explode(' ', $request->header('Authorization'));

            $user = User::where('remember_token', $token[1])->first();
            $employee = Employee::where('user_id', '=', $user->user_id)->first();

            if($employee){

                $status = ['surname' => $request->surname,
                'first_name' => $request->firstName,
                'middle_name' => $request->middleName,
                'extension' => $request->extension,
                'date_of_birth' => date('Y/m/d', strtotime($request->dateOfBirth)),
                'place_of_birth' => $request->placeOfBirth,
                'sex' => $request->sex,
                'civil_status' => $request->civilStatus,
                'avatar' => $request->avatar,
                'street' => $request->street,
                'city_or_municipality' => $request->city,
                'barangay' => $request->barangay,
                'zipcode' => $request->zipcode,
                'tel_no' => $request->telNo,
                'mobile_no' => $request->mobileNo,
                'email' => $request->email];

                if($request->exists('supervisor')){
                    $notification = new Notification;
                    $notification->employee_id = $employee->employee_id;
                    $notification->supervisor_id = $request->supervisor;
                    $notification->is_approved = 0;
                    $notification->save();
                }


                $placeOfAssignment = new TempPlaceOfAssignment;
                $placeOfAssignment->employee_id = $employee->employee_id;
                $placeOfAssignment->region_code = $request->regionCode;
                $placeOfAssignment->province_code = $request->provinceCode;
                $placeOfAssignment->city_code = $request->cityCode;
                $placeOfAssignment->position_id = $request->position_id;
                $placeOfAssignment->designation_id = $request->designation_id;

                // return $employee->province_code ."  ".$request->provinceCode;

                if($employee->region_code <> $request->regionCode || $employee->province_code <> $request->provinceCode || $employee->city_code <> $request->cityCode || $employee->position_id <> $request->position_id || $employee->designation_id <> $request->designation_id ){

                    $placeOfAssignment->save();
                }

                $logInEmployee = Employee::where('employee_id', $employee->employee_id)
                ->update($status);

                return response()->json([
                    'success' => 'true']);

            }else{

                return response()->json(['success' => 'false',
                    'message' => 'User Not Found']);
            }

        }else{

            return response()->json(['success' => 'false',
                'message' => 'No User Authentication Founded']);

        }

    }

    public function updatePassword(Request $request){

        if($request->headers->has('Authorization')){

            $token = explode(' ', $request->header('Authorization'));

            $user = User::where('remember_token', $token[1])->first();
            $employee = Employee::where('user_id', $user->user_id)->first();

            if($user){

                if($request->exists('confirmPassword') && $request->exists('newPassword')){

                    if (Hash::check($request->currentPassword, $user->password)) {
                        if($request->newPassword == $request->confirmPassword){

                            $password_array = ['password' => Hash::make($request->confirmPassword)];

                            $updatedPasswordEmployee = User::where('user_id', $employee->user_id)
                            ->update($password_array);

                            return response()->json([
                                'success' => 'true',
                                'employeeId' => $employee->employee_id,
                                'api_token' => $user->remember_token,
                                'message' => 'Saved new password'

                                ]);

                        }else{

                            return response()->json(['success' => 'false',
                                'message' => 'Password does not matched']);

                        }

                    }else{

                        return response()->json(['success' => 'false',
                            'message' => 'Invalid password']);

                    }

                }else{

                    return response()->json(['success' => 'false',
                        'message' => 'No password has been changed']);
                }

            }else{

                return response()->json(['success' => 'false',
                    'message' => 'Invalid User Authentication']);
            }

        }else{

            return response()->json(['success' => 'false',
                'message' => 'No User Authentication Founded']);

        }
    }

    //MET, UNMET, EXCEED Employee
    public function getEmployeeLevel(Request $request){

        if($request->headers->has('Authorization')){
            $token = explode(' ', $request->header('Authorization'));
            $user = User::where('remember_token', $token[1])->first();

            if($user){
                $employee = Employee::where('user_id', $user->user_id)->first();

                $expectedLevelQuery = DB::table(DB::raw('(SELECT AVG(employee_rate) AS employeeAverage, ANY_VALUE(employee_rate) AS employee_rate, ANY_VALUE(CASE cb.level WHEN 2 THEN 2 + 1 WHEN 3 THEN 3 + 1 ELSE cb.level END) AS expected_level, AVG(supervisor_rate) AS supervisorAverage, ANY_VALUE(supervisor_rate) AS supervisor_rate, ANY_VALUE(assessment_id) AS assessment_id, ANY_VALUE(employee_id) AS employee_id, ANY_VALUE(employee_assessments.behavioral_id) AS behavioral_id FROM employee_assessments LEFT JOIN (concept_behaviorals AS cb) ON employee_assessments.behavioral_id = cb.behavioral_id JOIN (concept_child_competencies) ON concept_child_competencies.concept_child_competency_id = cb.concept_child_competency_id WHERE employee_assessments.employee_id = '.$employee->employee_id. ' AND YEAR(employee_assessments.updated_at) = YEAR(CURDATE()) GROUP BY cb.level, concept_child_competencies.concept_child_competency_id , employee_assessments.employee_id) AS assess'))
                ->whereYear('employee_assessments.updated_at', '=', date('Y'))
                ->leftJoin('concept_behaviorals AS behaviorals', function ($join){
                    $join->on('behaviorals.behavioral_id', '=', 'assess.behavioral_id');
                })
                ->leftJoin('concept_child_competencies', function ($join){
                    $join->on('behaviorals.concept_child_competency_id', '=', 'concept_child_competencies.concept_child_competency_id');
                })
                ->leftJoin('concept_contexts', function ($join){
                    $join->on('concept_child_competencies.concept_child_competency_id', '=', 'concept_contexts.concept_child_competency_id');
                })
                ->leftJoin('concept_competencies', function ($join){
                    $join->on('concept_competencies.competency_id', '=', 'concept_child_competencies.concept_competencies_id');
                })
                ->leftJoin('employee_assessments', function ($join){
                    $join->on('behaviorals.behavioral_id', '=', 'employee_assessments.behavioral_id');
                })
                ->where('employee_assessments.employee_id', $employee->employee_id)
                ->orWhere(DB::raw('employee_assessments.employee_id'))
                ->select(DB::raw('ANY_VALUE(concept_competencies.competency_id), ANY_VALUE(concept_competencies.competency_name), ANY_VALUE(concept_child_competencies.concept_child_competency_id) AS concept_child_competency_id, ANY_VALUE(concept_child_competencies.name), CAST(ANY_VALUE((employeeAverage * .30) + (supervisorAverage * .70)) AS DECIMAL (10 , 2 )) AS totalAverage, IF(CAST(ANY_VALUE((employeeAverage * .30) + (supervisorAverage * .70)) AS DECIMAL (10 , 2 )) > "3.00" AND CAST(ANY_VALUE((employeeAverage * .30) + (supervisorAverage * .70)) AS DECIMAL (10 , 2 )) <= "5.00", "MET", "UNMET") AS status,  ANY_VALUE(expected_level) AS expected_level, ANY_VALUE(CASE expected_level WHEN 4 THEN 4 ELSE expected_level + 1 END) AS max_level'))
                ->groupBy('behaviorals.concept_child_competency_id')
                ->get();

                // return $expectedLevelQuery;

                $decodeExpectedLevel = json_decode($expectedLevelQuery, true);
                
                $minLevelArray = [];
                $expectedLevelArray = [];
                $maxLevelArray = [];

                $minLevelArrayResult = [];
                $expectedLevelArrayResult = [];
                $maxLevelArrayResult = [];
                
                $minLevelAverageQuery;
                $expectedLevelAverageQuery;
                $maxLevelAverageQuery;

                foreach ($decodeExpectedLevel as $key => $value) {
                    $expectedLevelArray[$value['concept_child_competency_id']] = $value['expected_level'];
                    $maxLevelArray[$value['concept_child_competency_id']] = $value['max_level'];
                }

                // return $expectedLevelArray; 

                // query for min result
                // foreach ($minLevelArray as $keyMin => $valueMin) {

                //  $minLevelAverageQuery = DB::table(DB::raw('(SELECT AVG(employee_rate) AS employeeAverage, ANY_VALUE(employee_rate) AS employee_rate, ANY_VALUE(level) AS expected_level, ANY_VALUE(case cb.level when 4 THEN 4 else max(cb.level) - min(cb.level) end) AS status_level, AVG(supervisor_rate) AS supervisorAverage, ANY_VALUE(supervisor_rate) AS supervisor_rate, ANY_VALUE(assessment_id) AS assessment_id, ANY_VALUE(employee_id) AS employee_id, ANY_VALUE(employee_assessments.behavioral_id) AS behavioral_id FROM employee_assessments LEFT JOIN (concept_behaviorals AS cb) ON employee_assessments.behavioral_id = cb.behavioral_id JOIN (concept_child_competencies) ON concept_child_competencies.concept_child_competency_id = cb.concept_child_competency_id WHERE employee_assessments.employee_id =' .$id. ' GROUP BY cb.level, concept_child_competencies.concept_child_competency_id , employee_assessments.employee_id) AS assess'))
                //  ->leftJoin('concept_behaviorals AS behaviorals', function ($join){
                //      $join->on('behaviorals.behavioral_id', '=', 'assess.behavioral_id');
                //  })
                //  ->leftJoin('concept_child_competencies', function ($join){
                //      $join->on('behaviorals.concept_child_competency_id', '=', 'concept_child_competencies.concept_child_competency_id');
                //  })
                //  ->leftJoin('concept_contexts', function ($join){
                //      $join->on('concept_child_competencies.concept_child_competency_id', '=', 'concept_contexts.concept_child_competency_id');
                //  })
                //  ->leftJoin('concept_competencies', function ($join){
                //      $join->on('concept_competencies.competency_id', '=', 'concept_child_competencies.concept_competencies_id');
                //  })
                //  ->leftJoin('employee_assessments', function ($join){
                //      $join->on('behaviorals.behavioral_id', '=', 'employee_assessments.behavioral_id');
                //  })
                //  ->where(function($query) use($id) {
                //      $query->where('employee_assessments.employee_id', $id)
                //      ->orWhere(DB::raw('employee_assessments.employee_id'));
                //  })

                //  ->where('behaviorals.level', $valueMin)
                //  ->select(DB::raw('ANY_VALUE(concept_competencies.competency_id) AS competency_id , ANY_VALUE(concept_competencies.competency_name) AS competency_name, ANY_VALUE(concept_child_competencies.name) AS child_competency_name, CAST(ANY_VALUE((employeeAverage * .30) + (supervisorAverage * .70)) AS DECIMAL (10 , 2 )) AS min_average, IF(CAST(ANY_VALUE((employeeAverage * .30) + (supervisorAverage * .70)) AS DECIMAL (10 , 2 )) > "3.00" AND CAST(ANY_VALUE((employeeAverage * .30) + (supervisorAverage * .70)) AS DECIMAL (10 , 2 )) <= "5.00", "MET", "UNMET") AS status, ANY_VALUE(case behaviorals.level when 4 THEN 4 else max(behaviorals.level) - min(behaviorals.level) end) AS expected_level, max(behaviorals.level) AS max_level, min(behaviorals.level) AS min_level, ANY_VALUE(behaviorals.level) AS level_value'))
                //  ->groupBy('behaviorals.concept_child_competency_id')
                //  ->offset($keyMin)
                //  ->limit(1)
                //  ->get();

                //  array_push($minLevelArrayResult, $minLevelAverageQuery);
                // }

                // return $minLevelArrayResult;

                foreach ($expectedLevelArray as $key2 => $value2) {

                    $expectedLevelAverageQuery = DB::table('concept_behaviorals')
                    ->whereYear('employee_assessments.updated_at', '=', date('Y'))
                    ->join('employee_assessments', 'concept_behaviorals.behavioral_id', '=', 'employee_assessments.behavioral_id')
                    ->leftJoin('concept_child_competencies', 'concept_behaviorals.concept_child_competency_id', '=', 'concept_child_competencies.concept_child_competency_id')
                    ->leftJoin('concept_competencies', 'concept_child_competencies.concept_competencies_id', '=', 'concept_competencies.competency_id')
                    ->where(function($query) use($employee) {
                        $query->where('employee_assessments.employee_id', $employee->employee_id)
                        ->orWhere(DB::raw('employee_assessments.employee_id'));
                    })

                    ->where('concept_behaviorals.level', $value2)
                    ->where('concept_child_competencies.concept_child_competency_id', $key2)
                    ->select(DB::raw('ANY_VALUE(concept_competencies.competency_name) AS competency_name, concept_child_competencies.concept_child_competency_id, concept_child_competencies.name, ANY_VALUE(concept_behaviorals.behavioral_id) AS behavioral_id, ANY_VALUE(concept_behaviorals.behavioral_name) AS behavioral_name, ANY_VALUE(concept_behaviorals.level) AS level, ANY_VALUE(employee_assessments.employee_rate) AS employee_rate, AVG(employee_assessments.employee_rate) AS total_employee_rate, CAST(AVG((employee_assessments.employee_rate * .30) + (employee_assessments.supervisor_rate * .70)) AS DECIMAL (10 , 2 )) AS expected_average'))
                    ->groupBy('concept_behaviorals.concept_child_competency_id')
                    ->get();
                    
                    array_push($expectedLevelArrayResult, $expectedLevelAverageQuery);
                }

                // return $expectedLevelArrayResult;

                // query for max result
                foreach ($maxLevelArray as $key3 => $value3) {

                    $maxLevelAverageQuery = DB::table('concept_behaviorals')
                    ->whereYear('employee_assessments.updated_at', '=', date('Y'))
                    ->join('employee_assessments', 'concept_behaviorals.behavioral_id', '=', 'employee_assessments.behavioral_id')
                    ->leftJoin('concept_child_competencies', 'concept_behaviorals.concept_child_competency_id', '=', 'concept_child_competencies.concept_child_competency_id')
                    ->leftJoin('concept_competencies', 'concept_child_competencies.concept_competencies_id', '=', 'concept_competencies.competency_id')
                    ->where(function($query) use($employee) {
                        $query->where('employee_assessments.employee_id', $employee->employee_id)
                        ->orWhere(DB::raw('employee_assessments.employee_id'));
                    })

                    ->where('concept_behaviorals.level', $value3)
                    ->where('concept_child_competencies.concept_child_competency_id', $key3)
                    ->select(DB::raw('ANY_VALUE(concept_competencies.competency_name) AS competency_name, concept_child_competencies.concept_child_competency_id, concept_child_competencies.name, ANY_VALUE(concept_behaviorals.behavioral_id) AS behavioral_id, ANY_VALUE(concept_behaviorals.behavioral_name) AS behavioral_name, ANY_VALUE(concept_behaviorals.level) AS level, ANY_VALUE(employee_assessments.employee_rate) AS employee_rate, AVG(employee_assessments.employee_rate) AS total_employee_rate, CAST(AVG((employee_assessments.employee_rate * .30) + (employee_assessments.supervisor_rate * .70)) AS DECIMAL (10 , 2 )) AS max_average'))
                    ->groupBy('concept_behaviorals.concept_child_competency_id')
                    ->get();

                    array_push($maxLevelArrayResult, $maxLevelAverageQuery);
                }

                // return $maxLevelArrayResult;

                $arrayResult = array_merge($minLevelArrayResult, $expectedLevelArrayResult, $maxLevelArrayResult);

                // return $arrayResult;

                $decodeArray;
                $responseArray = array();
                $responseArray2 = array();
                $exceededArray = array();
                $metArray = array();


                // Merge of two array into one key -> value pair
                foreach ($arrayResult as $key4 => $value4) {

                    $decodeArray = json_decode($value4, true);

                    // loop of decoded array
                    foreach ($decodeArray as $key5 => $value5) {

                        if (!isset($responseArray[$value5['competency_name']])) {
                            $responseArray[$value5['competency_name']] = array('competency_name' => $value5['competency_name']);
                        }


                        if (!isset($responseArray[$value5['competency_name']][$value5['name']])){
                            $responseArray[$value5['competency_name']][$value5['name']] = array('name' => $value5['name']);
                        }

                        // if(isset($value5['min_average'])){
                        //  $responseArray[$value5['competency_name']][$value5['child_competency_name']]['min_average'] = $value5['min_average'];
                        // }

                        if(isset($value5['expected_average'])){
                            $responseArray[$value5['competency_name']][$value5['name']]['expected_average'] = $value5['expected_average'];
                        }

                        if(isset($value5['max_average'])){
                            $responseArray[$value5['competency_name']][$value5['name']]['max_average'] = $value5['max_average'];
                        }

                    }
                }

                // map the met, unmet and exceeded
                foreach ($responseArray as $key6 => $value6) {

                    foreach ($value6 as $key7 => $value7) {

                        if(is_array($value7)){
                            if (!isset($responseArray2[$value6['competency_name']])) {
                                $responseArray2[$value6['competency_name']] = array(
                                    'competency_name' => $value6['competency_name']);
                            }

                            // if($value7['min_average'] > $value7['expected_average']){
                            //  echo " UNMET ";
                            //  if (!isset($responseArray2[$value6['competency_name']])) {
                            //      $responseArray2[$value6['competency_name']]['UNMET'][$value7['child_competency_name']] = array(
                            //          'child_competency_name' => $value7['child_competency_name']);
                            //  }

                            //  // echo $key8[$value8];

                            //  // var_dump($value8);

                            //  // var_dump($value8['min_average']);

                            //  // if($'min_average'] > $value6['expected_average']){
                            //  //  if (!isset($responseArray2[$value6['competency_name']])) {
                            //  //      $responseArray2[$value6['competency_name']]['UNMET'][$value6['child_competency_name']] = array(
                            //  //          'competency_name' => $value6['competency_name']);
                            //  //  }
                            //  // }
                            // }else{

                            if($value7['expected_average'] >= $value7['max_average']){
                                if($value7['expected_average'] >= 1 && $value7['expected_average'] <= 3.00){

                                    $responseArray2[$value6['competency_name']]['UNMET'][] = array('name' => $value7['name']);
                                    
                                    // echo " UNMET ";
                                }else{

                                    $responseArray2[$value6['competency_name']]['MET'][] = array('name' => $value7['name']);

                                    // echo " MET ";
                                }

                            }else{

                                if($value7['max_average'] >= 1 && $value7['max_average'] <= 3.00){

                                    $responseArray2[$value6['competency_name']]['UNMET'][] = array('name' => $value7['name']);
                                    
                                    // echo " UNMET ";
                                }else{
                                    $responseArray2[$value6['competency_name']]['EXCEEDED'][] = array('name' => $value7['name']);
                                }
                            }

                        }
                    }

                }

                return $responseArray2;

            } else {
                return response()->json(['success' => 'false',
                    'message' => 'User Not Found']);
            }

        }else{

            return response()->json(['success' => 'false',
                'message' => 'No User Authentication Founded']);
        }

    }  


    /**
    Admin
    */

    public function saveHRMOIII(Request $request){

        $employee = new Employee;
        $employee->surname = $request->surname;
        $employee->first_name = $request->firstName;
        $employee->middle_name = $request->middleName;
        $employee->extension = $request->extension;
        $employee->date_of_birth = date('Y/m/d', strtotime($request->dateOfBirth));
        $employee->place_of_birth = $request->placeOfBirth;
        $employee->sex = $request->sex;
        $employee->civil_status = $request->civilStatus;
        $employee->avatar = $request->avatar;
        $employee->street = $request->street;
        $employee->city_or_municipality = $request->city;
        $employee->barangay = $request->barangay;
        $employee->zipcode = $request->zipcode;
        $employee->tel_no = $request->telNo;
        $employee->mobile_no = $request->mobileNo;
        $employee->email = $request->email;
        $employee->user_id = $this->saveUserCredentials($request->firstName, $request->surname, $request->designation_id, $request->email)->user_id;
        $employee->region_code = $request->regionCode;
        $employee->province_code = $request->provinceCode;
        $employee->city_code = $request->cityCode;
        $employee->designation_id = $request->designation_id;
        $employee->position_id = $request->position_id;
        $employee->hrmo_is_approved = 1;
        $employee->save();

        return response()->json([
            'success' => 'true'
            ]);

    }

    public function getRegionalUsers(Request $request){
        if($request->headers->has('Authorization')){

            $token = explode(' ', $request->header('Authorization'));
            $user = User::where('remember_token', $token[1])->first();
            // $employee = Employee::where('user_id', '=', $user->user_id)->first();

            if($user){

                $query = Employee::where('employees.designation_id', 15)
                ->leftJoin('designations', 'employees.designation_id', '=', 'designations.designation_id')
                ->leftJoin('positions', 'employees.position_id','=', 'positions.position_id')
                ->leftJoin('province_assignments', 'province_assignments.province_code', '=', 'employees.province_code')
                ->leftJoin('city_assignments', 'city_assignments.city_code', '=', 'employees.city_code')
                ->get();

                return $query;

            }else{

                return response()->json(['success' => 'false',
                    'message' => 'User Not Found']);

            }
        }else{

            return response()->json(['success' => 'false',
                'message' => 'No User Authentication Founded']);

        }
    }

    public function getEmployeeData(Request $request){
        if($request->headers->has('Authorization')){

            $token = explode(' ', $request->header('Authorization'));
            $user = User::where('remember_token', $token[1])->first();
            // $employee = Employee::where('user_id', '=', $user->user_id)->first();

            if($user){

                $query = Employee::where('employees.employee_id', $request->employee_id)
                ->leftJoin('designations', 'employees.designation_id', '=', 'designations.designation_id')
                ->leftJoin('positions', 'employees.position_id','=', 'positions.position_id')
                ->leftJoin('province_assignments', 'province_assignments.province_code', '=', 'employees.province_code')
                ->leftJoin('city_assignments', 'city_assignments.city_code', '=', 'employees.city_code')
                ->get();

                return $query;

            }else{

                return response()->json(['success' => 'false',
                    'message' => 'User Not Found']);

            }
        }else{

            return response()->json(['success' => 'false',
                'message' => 'No User Authentication Founded']);

        }
    }

    public function updateHRMORegion(Request $request){
        if($request->headers->has('Authorization')){

            $token = explode(' ', $request->header('Authorization'));
            $user = User::where('remember_token', $token[1])->first();

            if($user){

                $employee = Employee::where('employee_id', '=', $request->employee_id)->first();

                $status = ['region_code' => $request->region_code,
                'province_code' => $request->province_code,
                'city_code' => $request->city_code,
                'supervisor' => null];

                if((isset($request->regionCode) && $employee->region_code <> $request->regionCode) || (isset($request->provinceCode) && $employee->province_code <> $request->provinceCode) || (isset($request->city_code) && $employee->city_code <> $request->cityCode)){

                    $employeeData = Employee::where('employee_id', $request->employee_id)
                    ->update($status);
                }

                return response()->json(['success' => 'true',
                    'message' => 'Successfully Update']);

            }else{

                return response()->json(['success' => 'false',
                    'message' => 'User Not Found']);

            }
        }else{

            return response()->json(['success' => 'false',
                'message' => 'No User Authentication Founded']);

        }
    }

    public function deactivateAccount(Request $request){
        if($request->headers->has('Authorization')){

            $token = explode(' ', $request->header('Authorization'));
            $user = User::where('remember_token', $token[1])->first();

            if($user){

                $query = Employee::where('employee_id', $request->employee_id)->first();
                if($query){
                    $deletedRows = Employee::where('employee_id', $query->employee_id)->delete();
                    $user = User::where('user_id', $query->user_id)->delete();
                }else{
                    return response()->json(['success' => 'false',
                        'message' => 'Employee does not exist']);                    
                }

                return response()->json(['success' => 'true',
                    'message' => 'Successfully Deactivate']);

            }else{

                return response()->json(['success' => 'false',
                    'message' => 'User Not Found']);

            }
        }else{

            return response()->json(['success' => 'false',
                'message' => 'No User Authentication Founded']);

        }
    }

    /**
    HRMO III Employee
    */

    public function getEmployeeForApprovalOfHRMOByRegion(Request $request){

        if($request->headers->has('Authorization')){

            $token = explode(' ', $request->header('Authorization'));
            $user = User::where('remember_token', $token[1])->first();
            $employee = Employee::where('user_id', '=', $user->user_id)->first();

            if($user){

                $query = Employee::where('employees.region_code', '=', $employee->region_code)
                ->where('hrmo_is_approved', 0)
                ->where('employees.designation_id', '!=', 15)
                ->leftJoin('designations', 'employees.designation_id', '=', 'designations.designation_id')
                ->leftJoin('positions', 'employees.position_id','=', 'positions.position_id')
                ->leftJoin('province_assignments', 'province_assignments.province_code', '=', 'employees.province_code')
                ->leftJoin('city_assignments', 'city_assignments.city_code', '=', 'employees.city_code')
                ->leftJoin('region_assignments', 'region_assignments.region_code', '=', 'employees.region_code')
                ->paginate(15);

                return response()->json([
                    'count' => count($query),
                    $query]);

            }else{

                return response()->json(['success' => 'false',
                    'message' => 'User Not Found']);

            }
        }else{

            return response()->json(['success' => 'false',
                'message' => 'No User Authentication Founded']);

        }
    }

    public function getApprovedEmployeeOfHRMOByRegion(Request $request){

        if($request->headers->has('Authorization')){

            $token = explode(' ', $request->header('Authorization'));
            $user = User::where('remember_token', $token[1])->first();
            $employee = Employee::where('user_id', '=', $user->user_id)->first();

            if($user){

                $query = Employee::where('employees.region_code', '=', $employee->region_code)
                ->where('hrmo_is_approved', 1)
                ->where('employees.designation_id', '!=', 15)
                ->leftJoin('designations', 'employees.designation_id', '=', 'designations.designation_id')
                ->leftJoin('positions', 'employees.position_id','=', 'positions.position_id')
                ->leftJoin('province_assignments', 'province_assignments.province_code', '=', 'employees.province_code')
                ->leftJoin('city_assignments', 'city_assignments.city_code', '=', 'employees.city_code')
                ->get();

                return $query;

            }else{

                return response()->json(['success' => 'false',
                    'message' => 'User Not Found']);

            }
        }else{

            return response()->json(['success' => 'false',
                'message' => 'No User Authentication Founded']);

        }
    }

    public function approveEmployeeRegistrationByHRMO(Request $request, $id){

        if($request->headers->has('Authorization')){

            $token = explode(' ', $request->header('Authorization'));

            $user = User::where('remember_token', $token[1])->first();
            $employee = Employee::where('user_id', '=', $user->user_id)->first();

            if($user){
                if(strcasecmp($request->status, 'Confirm') == 0){

                    $employeeData = Employee::where('employee_id', $id)->first();

                    $status = ['hrmo_is_approved' => 1,
                    'user_id' => $this->saveUserCredentials($employeeData->first_name, $employeeData->surname, $employeeData->designation_id, $employeeData->email)->user_id];

                    $logInEmployee = Employee::where('employee_id', $id)
                    ->where('region_code', $employee->region_code)
                    ->update($status);

                }else if(strcasecmp($request->status, 'Delete') == 0){

                    $deletedRows = Employee::where('employee_id', $id)->delete();
                }

                return response()->json([
                    'success' => 'true']);

            }else{

                return response()->json(['success' => 'false',
                    'message' => 'User Not Found']);
            }

        }else{

            return response()->json(['success' => 'false',
                'message' => 'No User Authentication Founded']);

        }
    }

    //GET
    //get the chages request for place of assignment by HRMO
    public function getChangeOfPlaceAssignmentRequest(Request $request){

        if($request->headers->has('Authorization')){

            $token = explode(' ', $request->header('Authorization'));
            $user = User::where('remember_token', $token[1])->first();
            $employee = Employee::where('user_id', '=', $user->user_id)->first();

            if($user){

                $query = TempPlaceOfAssignment::where('temp_place_of_assignments.region_code', '=', $employee->region_code)
                ->join('employees', 'employees.employee_id', '=', 'temp_place_of_assignments.employee_id')
                ->leftJoin('designations', 'employees.designation_id', '=', 'designations.designation_id')
                ->leftJoin('positions', 'temp_place_of_assignments.position_id','=', 'positions.position_id')
                ->leftJoin('region_assignments', 'region_assignments.region_code', '=', 'temp_place_of_assignments.region_code')
                ->leftJoin('province_assignments', 'province_assignments.province_code', '=', 'temp_place_of_assignments.province_code')
                ->leftJoin('city_assignments', 'city_assignments.city_code', '=', 'temp_place_of_assignments.city_code')
                ->select('temp_place_of_assignments.employee_id', 'surname', 'first_name', 'middle_name', 'region_assignments.region_name', 'province_assignments.province_name', 'city_assignments.city_name', 'positions.position_name', 'designations.designation_name')
                ->get();

                return $query;

            }else{

                return response()->json(['success' => 'false',
                    'message' => 'User Not Found']);

            }
        }else{

            return response()->json(['success' => 'false',
                'message' => 'No User Authentication Founded']);

        }
    }

    //POST
    //Approved the request
    public function approvedChangesOfPlaceOfAssignment(Request $request, $id){

        if($request->headers->has('Authorization')){

            $token = explode(' ', $request->header('Authorization'));

            $user = User::where('remember_token', $token[1])->first();
            $employee = Employee::where('user_id', '=', $user->user_id)->first();

            if($user){
                if(strcasecmp($request->status, 'Confirm') == 0){

                    $employeeData = TempPlaceOfAssignment::where('employee_id', $id)->first();

                    $status = ['region_code' => $employeeData->region_code,
                    'province_code' => $employeeData->province_code,
                    'city_code' => $employeeData->city_code,
                    'position_id' => $employeeData->position_id,
                    'designation_id' => $employeeData->designation_id];

                    $logInEmployee = Employee::where('employee_id', $id)
                    ->update($status);

                    DB::table('temp_place_of_assignments')->where('employee_id', $id)->delete();

                }else if(strcasecmp($request->status, 'Delete') == 0){

                    DB::table('temp_place_of_assignments')->where('employee_id', $id)->delete();
                }

                return response()->json([
                    'success' => 'true']);

            }else{

                return response()->json(['success' => 'false',
                    'message' => 'User Not Found']);
            }

        }else{

            return response()->json(['success' => 'false',
                'message' => 'No User Authentication Founded']);

        }

    }

    public function saveUserCredentials($firstName, $surname, $designationId, $email){

        // $employee = Employee::where('email', $email)->first();
        // if($employee){

        //     return response()->json(['success' => 'false',
        //         'message' => 'Email already Exists']);

        // }else{

            $firstInitial = camel_case($firstName);
            $lastName = str_replace(' ', '', $surname);
            $camelCase = camel_case($lastName);
            $nrRand = rand(0, 100);

            $username = $firstInitial[0].$camelCase.$nrRand;

            $user = new User;
       //check if the username is not null
            if($username != null){

            //search in the db if the username is exist
                $usernameEmployee = User::where('username', $username)->first();

            //return if the username already exist
                if($usernameEmployee){

                    return response()->json(['success' => 'false',
                        'message' => 'username already exists']);

                }

            }

            $user->username = $username;

            $random = str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890');
            $password = substr($random, 0, 10);

        //check if the password is not null
            if($password != null){
            //hash the password
                $user->password = Hash::make($password);

            }

            if($designationId == 15){
                $user->role_id = 2;
            }elseif ($designationId == 1 || $designationId == 2 || $designationId == 5 || $designationId == 6 || $designationId == 11) {
                $user->role_id = 3;
            }

            $to = "no-reply@gmail.com";
            $subject = "User Credentials";
            $headers = "From: no-reply@gmail.com" . "\r\n";
            $headers .= "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            $message = "
            <html>
            <head>
                <title>HTML email</title>
            </head>
            <body>
                <p>User Credentials</p>
                <table>
                    <tr>
                        <th>Username</th>
                        <th>Password</th>
                    </tr>
                    <tr>
                        <td>".$username."</td>
                        <td>".$password."</td>
                    </tr>
                </table>
                <p>Kindly change your password</p>
            </body>
            </html>
            ";

            mail($email,"User Registration",$message, $headers);
            $user->save();

            return $user;

        // }
    }

    public function sendMailTest(Request $request){

        $email = "rheyn26@gmail.com";

        $subject = "User Credentials";
        $headers = "From: test.lga26@gmail.com" . "\r\n";
        $headers .= "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        $message = "
        <html>
        <head>
            <title>HTML email</title>
        </head>
        <body>
            <p>User Credentials</p>
            <table>
                <tr>
                    <th>Username</th>
                    <th>Password</th>
                </tr>
                <tr>
                    <td>hello!!!</td>
                    <td>dkjfdkf</td>
                </tr>
            </table>
            <p>Kindly change your password</p>
        </body>
        </html>
        ";

        mail($email,"User Registration",$message, $headers, '-fwebsite@cd.lga.gov.ph');

        return "Success";
    }
}
