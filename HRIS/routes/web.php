<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function(){ 
	require(path("public")."login.html");
});

Auth::routes();

Route::resource('crm', 'EmployeeRegistrationController');
Route::post('crm/employee/updateEmployee', 'EmployeeRegistrationController@updateEmployeeData');
Route::get('crm/getSupervisor/designation', 'EmployeeRegistrationController@getSupervisorDesignation');
Route::get('crm/getSupervisor/{regionCode}/{designation}/{name}', 'EmployeeRegistrationController@getSupervisor');
Route::get('crm/employee/employeeLevel', 'EmployeeRegistrationController@getEmployeeLevel');

//ADMIN
Route::post('crm/saveRegional/HRMO', 'EmployeeRegistrationController@saveHRMOIII');
Route::get('crm/getRegional/HRMO', 'EmployeeRegistrationController@getRegionalUsers');
Route::post('crm/getEmployee/data', 'EmployeeRegistrationController@getEmployeeData');
Route::post('crm/updateRegional/HRMO', 'EmployeeRegistrationController@updateHRMORegion');

//HRMO
Route::get('crm/getApprovalEmployee/HRMO', 'EmployeeRegistrationController@getEmployeeForApprovalOfHRMOByRegion');
Route::get('crm/getApprovedEmployee/HRMO', 'EmployeeRegistrationController@getApprovedEmployeeOfHRMOByRegion');
Route::post('crm/confirmEmployee/status/{id}', 'EmployeeRegistrationController@approveEmployeeRegistrationByHRMO');
Route::get('crm/getChangeOfPlaceAssignmentRequest/HRMO', 'EmployeeRegistrationController@getChangeOfPlaceAssignmentRequest');
Route::post('crm/approvedChangesOfPlaceOfAssignment/status/{id}', 'EmployeeRegistrationController@approvedChangesOfPlaceOfAssignment');

Route::post('crm/employee/updatePassword', 'EmployeeRegistrationController@updatePassword');
Route::post('crm/user/addUser', 'UserRegistration@register');
Route::post('crm/user/deactivate', 'EmployeeRegistrationController@deactivateAccount');

//Get Region, Province and City URL
Route::get('/crm/getRegion/all', 'PlaceAssignmentController@getRegionPlace');
Route::get('/crm/getProvince/{regionCode}', 'PlaceAssignmentController@getProvinceByRegion');
Route::get('/crm/getCity/{provinceCode}', 'PlaceAssignmentController@getCityByProvince');
Route::get('/crm/getAllCity/{cityName}', 'PlaceAssignmentController@getAllCity');
Route::get('/crm/getAllProvince/{provinceName}', 'PlaceAssignmentController@getAllProvince');

//Get Designation and Position URL
Route::get('/crm/getPosition/all', 'DesignationAndPositionController@getPosition');
Route::get('/crm/getDesignation/{id}', 'DesignationAndPositionController@getDesignationByPosition');

//GET Designation and Postion for HRMO III
Route::get('/crm/getHRMOPosition/all', 'DesignationAndPositionController@getHRMOPosition');
Route::get('/crm/getHRMODesignation/{id}', 'DesignationAndPositionController@getHRMODesignationByPosition');

//Log-In and Log-Out
Route::post('crm/login/user', 'LogInEmployeeController@logInUser');
Route::post('crm/logout/user', 'LogInEmployeeController@logOutUser');
Route::post('crm/forgotPassword/user', 'LogInEmployeeController@forgotPassword');

Route::post('login', 'Auth\LoginController@login');

//Comptency
Route::get('crm/competency/list', 'CompetencyController@getCompetencyByPosition');
Route::post('crm/competency/saveCompetency', 'CompetencyController@saveCompetency');
Route::post('crm/competency/submitCompetency', 'CompetencyController@submitCompetency');
Route::post('crm/competency/checkEmployeeCompetency', 'CompetencyController@checkEmployeeAssessment');
Route::get('crm/competency/getCompetencyName', 'CompetencyController@getCompetencyName');
Route::get('crm/competency/getChildCompetency/{id}', 'CompetencyController@getChildCompetency');



//Personnel List
Route::get('crm/personnel/requests', 'PersonnelListController@getEmployeePerSupervisor');
Route::get('crm/personnel/approved', 'PersonnelListController@getApprovedEmployeeBySupervisor');
Route::post('crm/personnel/status/{id}', 'PersonnelListController@approveEmployeeRequest');
Route::get('crm/personnel/level/{id}', 'PersonnelListController@getEmployeeAverageLevel');

//Incident Report
Route::get('crm/incident/getIncidentReport', 'IncidentReportController@getIncidentReport');
Route::post('crm/incident/saveIncidentReport', 'IncidentReportController@saveIncidentReport');

//IDP
Route::get('crm/idp/getIdpOfEmployeeIdBySupervisorId/{employee_id}', 'IndividualDevelopmentPlanController@getIdpOfEmployeeIdBySupervisorId');
Route::get('crm/idp/getIdpByEmployeeId', 'IndividualDevelopmentPlanController@getIdpByEmployeeId');
Route::post('crm/idp/saveIdp', 'IndividualDevelopmentPlanController@saveIdp');

//Training Attended
Route::get('crm/training/getEmployeeTrainingAttended', 'EmployeeTrainingAttendedController@getEmployeeTrainingAttended');
Route::post('crm/training/saveEmployeeTrainingAttended', 'EmployeeTrainingAttendedController@saveEmployeeTrainingAttended');

//Report
Route::get('crm/report/getRegionFilter', 'PlaceAssignmentController@getRegionFilter');
Route::post('crm/report/getIndividualReport', 'ReportController@getIndividualReport');
Route::post('crm/report/getAggregateReport', 'ReportController@getAggregateReport');
Route::post('crm/report/getTopTenCompetencyMet', 'ReportController@getTopTenCompetencyMet');

//Notification
Route::get('crm/notification/numberOfRequest', 'RoleController@countRequestForApproval');



//TEST MAIL
Route::post('crm/mail/sendMail', 'EmployeeRegistrationController@sendMailTest');